// Import vue component
import component from './src/DateTimePicker.vue';

// To allow use as module (npm/webpack/etc.) export component
export default component;
