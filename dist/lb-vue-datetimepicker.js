(function (global, factory) {
    typeof exports === 'object' && typeof module !== 'undefined' ? factory(exports, require('moment')) :
    typeof define === 'function' && define.amd ? define(['exports', 'moment'], factory) :
    (global = global || self, factory(global.LbVueDateTimePicker = {}, global.moment));
}(this, (function (exports, moment) { 'use strict';

    moment = moment && Object.prototype.hasOwnProperty.call(moment, 'default') ? moment['default'] : moment;

    //

    var script = {
        name: 'DateTimePicker',
        props: [
            'value',
            'inputClass',
            'translateMethod',
            'type',
            'format',
            'displayFormat',
            'minYear',
            'maxYear',
            'simple',
            'isUTC',
            'start',
            'end',
            'isClearable',
            'disabled',
            'showSeconds' ],
        data: function data()
        {
            var me = this;
            return {
                gettype: '',
                show: '',
                calcDate: null,
                showPicker: false,
                minYearDef: 1900,
                maxYearDef: 2100,
                startCalDate: null,
                endCalDate: null,
                isStart: null,
                isEnd: null,
                top: '',
                left: '',
                right: '',
                height: '',
                weekDays: [
                    {
                        key: '1',
                        text: me.translate('Mon'),
                    },
                    {
                        key: '2',
                        text: me.translate('Tue'),
                    },
                    {
                        key: '3',
                        text: me.translate('Wed'),
                    },
                    {
                        key: '4',
                        text: me.translate('Thu'),
                    },
                    {
                        key: '5',
                        text: me.translate('Fri'),
                    },
                    {
                        key: '6',
                        text: me.translate('Sat'),
                    },
                    {
                        key: '0',
                        text: me.translate('Sun'),
                    }],
                monthDays: [],
            };
        },
        created: function created()
        {
            var me = this;

            me.setValue();

            me.setStartEndValues();

            if (me.type === undefined)
            {
                me.gettype = 'datetime';
            }
            else
            {
                me.gettype = me.type;
            }

            if (me.minYear !== undefined)
            {
                me.minYearDef = parseInt(me.minYear, 10);
            }
            if (me.maxYear !== undefined)
            {
                me.maxYearDef = parseInt(me.maxYear, 10);
            }

            window.addEventListener('mousedown', me.windowEvt);
            window.addEventListener('scroll', me.scrollHandler);
            window.addEventListener('resize', me.reposition);
        },
        destroyed: function destroyed()
        {
            var me = this;

            window.removeEventListener('mousedown', me.windowEvt);
            window.removeEventListener('scroll', me.scrollHandler);
            window.removeEventListener('resize', me.reposition);
        },
        watch:
        {
            value: function value()
            {
                var me = this;
                me.setValue();
                me.calculateCalendar();
            },
            calcDate:
            {
                handler: function handler()
                {
                    var me = this;
                    me.calculateCalendar();
                },
                deep: true,
            },
            gettype: function gettype(val)
            {
                var me = this;

                if (val === 'time')
                {
                    me.centerValues();
                }
            },
            show: function show(val)
            {
                var me = this;

                if (val === 'year')
                {
                    me.centerValues();
                }
            },
            showPicker: function showPicker(val)
            {
                var me = this;

                if (val)
                {
                    me.show = 'date';
                    if (me.type === undefined)
                    {
                        me.gettype = 'datetime';
                    }
                    else
                    {
                        me.gettype = me.type;
                    }

                    me.$nextTick(function () {
                        me.scrollHandler();
                    });
                }
            },
            start: function start()
            {
                var me = this;
                me.setStartEndValues();
            },
            end: function end()
            {
                var me = this;
                me.setStartEndValues();
            },
        },
        methods:
        {
            isActiveDay: function isActiveDay()
            {
                var me = this;
                for (var week of me.monthDays)
                {
                    for (var day of week)
                    {
                        if (day.active === true && day.date.format('YYYY-MM-DD') === me.calcDate.format('YYYY-MM-DD'))
                        {
                            return true;
                        }
                    }
                }
                return false;
            },
            setStartEndValues: function setStartEndValues()
            {
                var me = this;

                if (me.start !== undefined)
                {
                    me.startCalDate = me.parseValue(me.start);
                    me.isEnd = true;
                }

                if (me.end !== undefined)
                {
                    me.endCalDate = me.parseValue(me.end);
                    me.isStart = true;
                }

                me.calculateCalendar();
            },
            setValue: function setValue()
            {
                var me = this;

                me.calcDate = me.parseValue(me.value);
            },
            parseValue: function parseValue(value)
            {
                var me = this;
                if (value == null || value === undefined)
                {
                    return moment();
                }

                if (me.format)
                {
                    if (me.isUTC)
                    {
                        return moment.utc(value, me.format).local();
                    }

                    return moment(value, me.format);
                }

                if (me.isUTC)
                {
                    return moment.utc(value).local();
                }

                var val = moment(value);

                if (!val.isValid())
                {
                    return moment();
                }

                return moment();
            },
            windowEvt: function windowEvt(evt)
            {
                var me = this;

                if (me.showPicker === true)
                {
                    if (me.$refs.lb_input === evt.target)
                    {
                        return;
                    }
                    
                    var ref = evt.composedPath();
                    var clickedElement = ref[0];

                    if (clickedElement
                    && me.recursChildNodes(me.$refs['lb-vue-cal_calendar'].childNodes, clickedElement))
                    {
                        return;
                    }

                    me.close();
                }
            },
            scrollHandler: function scrollHandler()
            {
                var me = this;
                var ticking = false;
                var offset = 0;
                if (!ticking)
                {
                    window.requestAnimationFrame(function () {
                        if (me.$refs['lb-vue-cal_cont'] !== undefined)
                        {
                            var calCont = me.$refs['lb-vue-cal_cont'];

                            var target = me.$refs.lb_input;

                            if (target.getBoundingClientRect().bottom + calCont.clientHeight > window.innerHeight)
                            {
                                offset = -calCont.clientHeight + -target.getBoundingClientRect().height;
                            }

                            var inputElementBottomPosition = me.$refs.lb_input.getBoundingClientRect().bottom;
                            me.top = (inputElementBottomPosition + offset) + "px";

                            ticking = false;
                        }
                    });

                    ticking = true;
                }
            },
            recursChildNodes: function recursChildNodes(childNodes, target)
            {
                var me = this;
                for (var node of childNodes)
                {
                    if (node === target)
                    {
                        return true;
                    }

                    if (me.recursChildNodes(node.childNodes, target))
                    {
                        return true;
                    }
                }
                return false;
            },
            centerValues: function centerValues()
            {
                var me = this;
                me.$nextTick(function () {
                    if (me.$refs.lb_dtp_selected)
                    {
                        for (var ref of me.$refs.lb_dtp_selected)
                        {
                            ref.scrollIntoView({ block: 'center' });
                        }
                    }
                });
            },
            close: function close()
            {
                var me = this;

                if (me.showPicker === true)
                {
                    me.showPicker = false;
                    if (!me.simple)
                    {
                        me.$refs.lb_input.blur();
                    }
                }
            },
            open: function open()
            {
                var me = this;
                me.reposition();
                me.showPicker = true;
            },
            reposition: function reposition()
            {
                var me = this;
                var simpleCalendarWidth = 400;
                var target = me.$refs.lb_input;

                if (target.getBoundingClientRect().left + simpleCalendarWidth > window.innerWidth)
                {
                    me.right = '0px';
                    me.left = '';
                }
                else
                {
                    me.right = '';
                    me.left = (target.getBoundingClientRect().left) + "px";
                }
            },
            changeMonth: function changeMonth(by)
            {
                var me = this;
                me.calcDate = moment(me.calcDate.add(by, 'month'));
            },
            translate: function translate(val)
            {
                var me = this;

                if (me.translateMethod !== undefined)
                {
                    return me.translateMethod(val);
                }
                return val;
            },
            setDate: function setDate()
            {
                var me = this;

                if (me.gettype === 'datetime')
                {
                    me.gettype = 'time';
                }
                else if (me.gettype === 'date' || me.gettype === 'time')
                {
                    me.sendDate();
                }
            },
            getCalcDate: function getCalcDate()
            {
                var me = this;
                return me.calcDate;
            },
            sendDate: function sendDate(closePicker)
            {
                if ( closePicker === void 0 ) closePicker = true;

                var me = this;
                if (me.format !== undefined)
                {
                    if (me.isUTC)
                    {
                        me.$emit('input', me.calcDate.utc().format(me.format));
                    }
                    else
                    {
                        me.$emit('input', me.calcDate.format(me.format));
                    }
                }
                else
                if (me.isUTC)
                {
                    me.$emit('input', me.calcDate.utc());
                }
                else
                {
                    me.$emit('input', me.calcDate);
                }
                if (closePicker)
                {
                    me.showPicker = false;
                }
            },
            changeDate: function changeDate(instance)
            {
                var me = this;

                if (instance.active !== undefined && instance.active === false)
                {
                    return;
                }

                me.calcDate = moment(instance.date);
            },
            clearPicker: function clearPicker()
            {
                var me = this;
                me.$emit('input', null);
            },
            calculateCalendar: function calculateCalendar()
            {
                var me = this;
                var setDate = me.calcDate;
                var date = moment();
                var i;

                var startDayMonth = moment(setDate).startOf('month').format('d') - 1;
                var endDayMonth = moment(setDate).endOf('month').format('d') - 1;

                if (startDayMonth === -1)
                {
                    startDayMonth = 6;
                }
                if (endDayMonth === -1)
                {
                    endDayMonth = 6;
                }

                var daysInMonth = moment(setDate).daysInMonth();
                var startDate = moment(setDate).startOf('month');
                var endDate = moment(setDate).endOf('month');

                var days = [];
                for (i = startDayMonth; i > 0; i -= 1)
                {
                    date = moment(startDate).subtract(i, 'days');

                    var active = true;
                    if (me.isStart && me.endCalDate < date)
                    {
                        active = false;
                    }

                    if (me.isEnd && me.startCalDate > date)
                    {
                        active = false;
                    }

                    days.push(
                        {
                            date: date,
                            text: date.format('DD'),
                            active: active,
                            class: 'previous-month',
                        }
                    );
                }

                for (i = 0; i < daysInMonth; i += 1)
                {
                    date = moment(startDate).add(i, 'days');

                    var active$1 = true;
                    if (me.isStart && me.endCalDate < date)
                    {
                        active$1 = false;
                    }

                    if (me.isEnd && me.startCalDate > date)
                    {
                        active$1 = false;
                    }

                    days.push(
                        {
                            date: date,
                            text: date.format('DD'),
                            active: active$1,
                            class: ("current-month" + ((date.format('YYYY-MM-DD') === setDate.format('YYYY-MM-DD')) ? ' active' : '')),
                        }
                    );
                }

                var x = 1;
                for (i = endDayMonth; i < 6; i += 1)
                {
                    date = moment(endDate).add(x, 'days');

                    var active$2 = true;
                    if (me.isStart && me.endCalDate < date)
                    {
                        active$2 = false;
                    }

                    if (me.isEnd && me.startCalDate > date)
                    {
                        active$2 = false;
                    }

                    days.push(
                        {
                            date: date,
                            text: date.format('DD'),
                            active: active$2,
                            class: 'next-month',
                        }
                    );
                    x += 1;
                }

                me.monthDays = [];

                var j; var temparray; var
                    chunk = 7;
                for (i = 0, j = days.length; i < j; i += chunk)
                {
                    temparray = days.slice(i, i + chunk);
                    me.monthDays.push(temparray);
                }
            },
        },
        computed:
        {
            displayValue:
            {
                get: function get()
                {
                    var me = this;

                    if (me.displayFormat !== undefined)
                    {
                        if (me.value == null || me.value === undefined)
                        {
                            return me.value;
                        }
                        if (me.format)
                        {
                            if (me.isUTC)
                            {
                                return moment.utc(me.value, me.format).local().format(me.displayFormat);
                            }
                            return moment(me.value, me.format).format(me.displayFormat);
                        }
                        if (me.isUTC)
                        {
                            return moment.utc(me.value).local().format(me.displayFormat);
                        }
                        return moment(me.value).format(me.displayFormat);
                    }
                    return me.value;
                },
                set: function set(newValue)
                {
                    var me = this;

                    if (moment(newValue, me.displayFormat, true).isValid())
                    {
                        var sel = me.$refs.lb_input.selectionStart;
                        me.calcDate = moment(newValue, me.displayFormat);
                        me.sendDate(false);
                        me.$nextTick(function () {
                            me.$refs.lb_input.setSelectionRange(sel, sel);
                        });
                    }
                },
            },
            month: function month()
            {
                var me = this;
                return me.translate(me.calcDate.format('MMMM').toUpperCase());
            },
            months: function months()
            {
                var me = this;
                var months = [];

                var currMonth = moment(me.calcDate).month();
                for (var i = 0; i < 12; i += 1)
                {
                    var date = moment(me.calcDate).month(i);

                    var active = true;
                    if (me.isStart && me.endCalDate < date)
                    {
                        active = false;
                    }

                    if (me.isEnd && me.startCalDate > date)
                    {
                        active = false;
                    }

                    months.push({
                        text: me.translate(date.format('MMM').toUpperCase()),
                        date: date,
                        active: active,
                        selected: (currMonth === i),
                    });
                }

                return months;
            },
            years: function years()
            {
                var me = this;
                var years = [];

                var currYear = moment(me.calcDate).year();
                for (var i = me.minYearDef; i < me.maxYearDef; i += 1)
                {
                    var date = moment(me.calcDate).year(i);

                    var active = true;
                    if (me.isStart && me.endCalDate < date)
                    {
                        active = false;
                    }

                    if (me.isEnd && me.startCalDate > date)
                    {
                        active = false;
                    }

                    years.push({
                        text: date.format('YYYY'),
                        date: date,
                        active: active,
                        selected: (currYear === i),
                    });
                }

                return years;
            },
            year: function year()
            {
                var me = this;
                return me.calcDate.format('YYYY');
            },
            hours: function hours()
            {
                var me = this;
                var hours = [];
                var currHour = moment(me.calcDate).hour();

                for (var i = 0; i < 24; i += 1)
                {
                    var date = moment(me.calcDate).hour(i);
                    var active = true;
                    if (me.isStart && me.endCalDate < date)
                    {
                        active = false;
                    }

                    if (me.isEnd && me.startCalDate > date)
                    {
                        active = false;
                    }

                    // check if hours is already in array (indicates daylight saving)
                    if (hours.slice(-1)[0] && hours.slice(-1)[0].text === date.format('HH'))
                    {
                        hours.splice(-1);
                    }

                    hours.push({
                        text: date.format('HH'),
                        date: date,
                        active: active,
                        selected: (currHour === i),
                    });
                }
                return hours;
            },
            minutes: function minutes()
            {
                var me = this;
                var minutes = [];

                var currMin = moment(me.calcDate).minute();
                for (var i = 0; i < 60; i += 1)
                {
                    var date = moment(me.calcDate).minute(i);

                    var active = true;
                    if (me.isStart && me.endCalDate < date)
                    {
                        active = false;
                    }

                    if (me.isEnd && me.startCalDate > date)
                    {
                        active = false;
                    }
                    minutes.push({
                        text: date.format('mm'),
                        date: date,
                        active: active,
                        selected: (currMin === i),
                    });
                }

                return minutes;
            },
            seconds: function seconds()
            {
                var me = this;
                var seconds = [];

                var currSec = moment(me.calcDate).second();
                for (var i = 0; i < 60; i += 1)
                {
                    var date = moment(me.calcDate).second(i);
                    var active = true;
                    if (me.isStart && me.endCalDate < date)
                    {
                        active = false;
                    }

                    if (me.isEnd && me.startCalDate > date)
                    {
                        active = false;
                    }
                    seconds.push({
                        text: date.format('ss'),
                        date: date,
                        active: active,
                        selected: (currSec === i),
                    });
                }

                return seconds;
            },
        },
    };

    function normalizeComponent(template, style, script, scopeId, isFunctionalTemplate, moduleIdentifier
    /* server only */
    , shadowMode, createInjector, createInjectorSSR, createInjectorShadow) {
      if (typeof shadowMode !== 'boolean') {
        createInjectorSSR = createInjector;
        createInjector = shadowMode;
        shadowMode = false;
      } // Vue.extend constructor export interop.


      var options = typeof script === 'function' ? script.options : script; // render functions

      if (template && template.render) {
        options.render = template.render;
        options.staticRenderFns = template.staticRenderFns;
        options._compiled = true; // functional template

        if (isFunctionalTemplate) {
          options.functional = true;
        }
      } // scopedId


      if (scopeId) {
        options._scopeId = scopeId;
      }

      var hook;

      if (moduleIdentifier) {
        // server build
        hook = function hook(context) {
          // 2.3 injection
          context = context || // cached call
          this.$vnode && this.$vnode.ssrContext || // stateful
          this.parent && this.parent.$vnode && this.parent.$vnode.ssrContext; // functional
          // 2.2 with runInNewContext: true

          if (!context && typeof __VUE_SSR_CONTEXT__ !== 'undefined') {
            context = __VUE_SSR_CONTEXT__;
          } // inject component styles


          if (style) {
            style.call(this, createInjectorSSR(context));
          } // register component module identifier for async chunk inference


          if (context && context._registeredComponents) {
            context._registeredComponents.add(moduleIdentifier);
          }
        }; // used by ssr in case component is cached and beforeCreate
        // never gets called


        options._ssrRegister = hook;
      } else if (style) {
        hook = shadowMode ? function () {
          style.call(this, createInjectorShadow(this.$root.$options.shadowRoot));
        } : function (context) {
          style.call(this, createInjector(context));
        };
      }

      if (hook) {
        if (options.functional) {
          // register for functional component in vue file
          var originalRender = options.render;

          options.render = function renderWithStyleInjection(h, context) {
            hook.call(context);
            return originalRender(h, context);
          };
        } else {
          // inject component registration as beforeCreate hook
          var existing = options.beforeCreate;
          options.beforeCreate = existing ? [].concat(existing, hook) : [hook];
        }
      }

      return script;
    }

    var normalizeComponent_1 = normalizeComponent;

    var isOldIE = typeof navigator !== 'undefined' && /msie [6-9]\\b/.test(navigator.userAgent.toLowerCase());
    function createInjector(context) {
      return function (id, style) {
        return addStyle(id, style);
      };
    }
    var HEAD = document.head || document.getElementsByTagName('head')[0];
    var styles = {};

    function addStyle(id, css) {
      var group = isOldIE ? css.media || 'default' : id;
      var style = styles[group] || (styles[group] = {
        ids: new Set(),
        styles: []
      });

      if (!style.ids.has(id)) {
        style.ids.add(id);
        var code = css.source;

        if (css.map) {
          // https://developer.chrome.com/devtools/docs/javascript-debugging
          // this makes source maps inside style tags work properly in Chrome
          code += '\n/*# sourceURL=' + css.map.sources[0] + ' */'; // http://stackoverflow.com/a/26603875

          code += '\n/*# sourceMappingURL=data:application/json;base64,' + btoa(unescape(encodeURIComponent(JSON.stringify(css.map)))) + ' */';
        }

        if (!style.element) {
          style.element = document.createElement('style');
          style.element.type = 'text/css';
          if (css.media) { style.element.setAttribute('media', css.media); }
          HEAD.appendChild(style.element);
        }

        if ('styleSheet' in style.element) {
          style.styles.push(code);
          style.element.styleSheet.cssText = style.styles.filter(Boolean).join('\n');
        } else {
          var index = style.ids.size - 1;
          var textNode = document.createTextNode(code);
          var nodes = style.element.childNodes;
          if (nodes[index]) { style.element.removeChild(nodes[index]); }
          if (nodes.length) { style.element.insertBefore(textNode, nodes[index]); }else { style.element.appendChild(textNode); }
        }
      }
    }

    var browser = createInjector;

    /* script */
    var __vue_script__ = script;

    /* template */
    var __vue_render__ = function() {
      var _vm = this;
      var _h = _vm.$createElement;
      var _c = _vm._self._c || _h;
      return _c(
        "div",
        {
          ref: "lb-vue-cal_calendar",
          staticClass: "lb-vue-cal_calendar",
          attrs: { tabindex: "0" },
          on: {
            keydown: function($event) {
              if (
                !$event.type.indexOf("key") &&
                _vm._k($event.keyCode, "esc", 27, $event.key, ["Esc", "Escape"])
              ) {
                return null
              }
              return _vm.close.apply(null, arguments)
            }
          }
        },
        [
          _c("input", {
            directives: [
              {
                name: "model",
                rawName: "v-model",
                value: _vm.displayValue,
                expression: "displayValue"
              }
            ],
            ref: "lb_input",
            class: _vm.inputClass,
            attrs: { type: "text", disabled: _vm.disabled },
            domProps: { value: _vm.displayValue },
            on: {
              focus: function($event) {
                $event.stopPropagation();
                return _vm.open.apply(null, arguments)
              },
              input: function($event) {
                if ($event.target.composing) {
                  return
                }
                _vm.displayValue = $event.target.value;
              }
            }
          }),
          _vm._v(" "),
          _vm.disabled !== true &&
          _vm.isClearable &&
          _vm.value !== null &&
          _vm.value !== undefined
            ? _c(
                "button",
                {
                  staticClass: "clear",
                  attrs: { disabled: _vm.disabled },
                  on: { click: _vm.clearPicker }
                },
                [_vm._v("X")]
              )
            : _vm._e(),
          _vm._v(" "),
          _vm.showPicker
            ? _c(
                "div",
                {
                  ref: "lb-vue-cal_cont",
                  class: { simple_cal_cont: _vm.simple },
                  style: {
                    top: _vm.top,
                    left: _vm.left,
                    right: _vm.right,
                    height: _vm.height + "px"
                  }
                },
                [
                  !_vm.simple
                    ? _c("div", {
                        staticClass: "lb-vue-cal_overlay",
                        on: { click: _vm.close }
                      })
                    : _vm._e(),
                  _vm._v(" "),
                  _c(
                    "div",
                    {
                      ref: "lb-vue-cal_calview",
                      class: { simple_cal: _vm.simple }
                    },
                    [
                      _vm.show == "date" &&
                      (_vm.gettype == "datetime" || _vm.gettype == "date")
                        ? _c("div", { staticClass: "lb-vue-cal_calendar" }, [
                            _c(
                              "div",
                              { staticClass: "lb-vue-cal_row calendar-head" },
                              [
                                _c(
                                  "div",
                                  {
                                    staticClass: "lb-vue-cal_arrow",
                                    on: {
                                      click: function($event) {
                                        $event.stopPropagation();
                                        return _vm.changeMonth(-1)
                                      }
                                    }
                                  },
                                  [
                                    _c(
                                      "svg",
                                      {
                                        attrs: {
                                          width: "8px",
                                          height: "12px",
                                          viewBox: "0 0 8 12",
                                          version: "1.1",
                                          xmlns: "http://www.w3.org/2000/svg",
                                          "xmlns:xlink":
                                            "http://www.w3.org/1999/xlink"
                                        }
                                      },
                                      [
                                        _c("title", [_vm._v("Group 11")]),
                                        _vm._v(" "),
                                        _c("desc", [
                                          _vm._v("Created with Sketch.")
                                        ]),
                                        _vm._v(" "),
                                        _c("defs"),
                                        _vm._v(" "),
                                        _c(
                                          "g",
                                          {
                                            attrs: {
                                              id: "Page-1",
                                              stroke: "none",
                                              "stroke-width": "1",
                                              fill: "none",
                                              "fill-rule": "evenodd"
                                            }
                                          },
                                          [
                                            _c(
                                              "g",
                                              {
                                                attrs: {
                                                  id: "Artboard",
                                                  transform:
                                                    "translate(-32.000000, -30.000000)"
                                                }
                                              },
                                              [
                                                _c(
                                                  "g",
                                                  {
                                                    attrs: {
                                                      id: "Group-12",
                                                      transform:
                                                        "translate(24.000000, 24.000000)"
                                                    }
                                                  },
                                                  [
                                                    _c(
                                                      "g",
                                                      { attrs: { id: "Group-11" } },
                                                      [
                                                        _c(
                                                          "g",
                                                          {
                                                            attrs: {
                                                              id:
                                                                "icons/ic_chevron_left"
                                                            }
                                                          },
                                                          [
                                                            _c(
                                                              "g",
                                                              {
                                                                attrs: {
                                                                  id:
                                                                    "ic_chevron_left"
                                                                }
                                                              },
                                                              [
                                                                _c(
                                                                  "g",
                                                                  {
                                                                    attrs: {
                                                                      id:
                                                                        "Icon-24px"
                                                                    }
                                                                  },
                                                                  [
                                                                    _c("polygon", {
                                                                      attrs: {
                                                                        id: "Shape",
                                                                        "fill-opacity":
                                                                          "0.9",
                                                                        fill:
                                                                          "#FFFFFF",
                                                                        points:
                                                                          "15.41 7.41 14 6 8 12 14 18 15.41 16.59 10.83 12"
                                                                      }
                                                                    }),
                                                                    _vm._v(" "),
                                                                    _c("polygon", {
                                                                      attrs: {
                                                                        id: "Shape",
                                                                        points:
                                                                          "0 0 24 0 24 24 0 24"
                                                                      }
                                                                    })
                                                                  ]
                                                                )
                                                              ]
                                                            )
                                                          ]
                                                        )
                                                      ]
                                                    )
                                                  ]
                                                )
                                              ]
                                            )
                                          ]
                                        )
                                      ]
                                    )
                                  ]
                                ),
                                _vm._v(" "),
                                _c("div", { staticClass: "month" }, [
                                  _c(
                                    "div",
                                    {
                                      on: {
                                        click: function($event) {
                                          $event.stopPropagation();
                                          _vm.show = "month";
                                        }
                                      }
                                    },
                                    [_vm._v(_vm._s(_vm.month))]
                                  ),
                                  _vm._v(" "),
                                  _c(
                                    "div",
                                    {
                                      staticClass:
                                        "lb-vue-cal_arrow drop-down-arrow",
                                      on: {
                                        click: function($event) {
                                          $event.stopPropagation();
                                          _vm.show = "month";
                                        }
                                      }
                                    },
                                    [
                                      _c(
                                        "svg",
                                        {
                                          attrs: {
                                            width: "10px",
                                            height: "5px",
                                            viewBox: "0 0 10 5",
                                            version: "1.1",
                                            xmlns: "http://www.w3.org/2000/svg",
                                            "xmlns:xlink":
                                              "http://www.w3.org/1999/xlink"
                                          }
                                        },
                                        [
                                          _c("title", [
                                            _vm._v("icons/ic_arrow_drop_down")
                                          ]),
                                          _vm._v(" "),
                                          _c("desc", [
                                            _vm._v("Created with Sketch.")
                                          ]),
                                          _vm._v(" "),
                                          _c("defs"),
                                          _vm._v(" "),
                                          _c(
                                            "g",
                                            {
                                              attrs: {
                                                id: "Page-1",
                                                stroke: "none",
                                                "stroke-width": "1",
                                                fill: "none",
                                                "fill-rule": "evenodd"
                                              }
                                            },
                                            [
                                              _c(
                                                "g",
                                                {
                                                  attrs: {
                                                    id: "Artboard",
                                                    transform:
                                                      "translate(-283.000000, -34.000000)"
                                                  }
                                                },
                                                [
                                                  _c(
                                                    "g",
                                                    {
                                                      attrs: {
                                                        id: "Group-Copy",
                                                        transform:
                                                          "translate(231.000000, 24.000000)"
                                                      }
                                                    },
                                                    [
                                                      _c(
                                                        "g",
                                                        {
                                                          attrs: {
                                                            id: "Group-14",
                                                            transform:
                                                              "translate(45.000000, 0.000000)"
                                                          }
                                                        },
                                                        [
                                                          _c(
                                                            "g",
                                                            {
                                                              attrs: {
                                                                id:
                                                                  "icons/ic_arrow_drop_down"
                                                              }
                                                            },
                                                            [
                                                              _c(
                                                                "g",
                                                                {
                                                                  attrs: {
                                                                    id:
                                                                      "ic_arrow_drop_down"
                                                                  }
                                                                },
                                                                [
                                                                  _c(
                                                                    "g",
                                                                    {
                                                                      attrs: {
                                                                        id:
                                                                          "Icon-24px"
                                                                      }
                                                                    },
                                                                    [
                                                                      _c(
                                                                        "polygon",
                                                                        {
                                                                          attrs: {
                                                                            id:
                                                                              "Shape",
                                                                            "fill-opacity":
                                                                              "0.9",
                                                                            fill:
                                                                              "#FFFFFF",
                                                                            points:
                                                                              "7 10 12 15 17 10"
                                                                          }
                                                                        }
                                                                      ),
                                                                      _vm._v(" "),
                                                                      _c(
                                                                        "polygon",
                                                                        {
                                                                          attrs: {
                                                                            id:
                                                                              "Shape",
                                                                            points:
                                                                              "0 0 24 0 24 24 0 24"
                                                                          }
                                                                        }
                                                                      )
                                                                    ]
                                                                  )
                                                                ]
                                                              )
                                                            ]
                                                          )
                                                        ]
                                                      )
                                                    ]
                                                  )
                                                ]
                                              )
                                            ]
                                          )
                                        ]
                                      )
                                    ]
                                  )
                                ]),
                                _vm._v(" "),
                                _c("div", { staticClass: "year" }, [
                                  _c(
                                    "div",
                                    {
                                      on: {
                                        click: function($event) {
                                          $event.stopPropagation();
                                          _vm.show = "year";
                                        }
                                      }
                                    },
                                    [_vm._v(_vm._s(_vm.year))]
                                  ),
                                  _vm._v(" "),
                                  _c(
                                    "div",
                                    {
                                      staticClass:
                                        "lb-vue-cal_arrow drop-down-arrow",
                                      on: {
                                        click: function($event) {
                                          $event.stopPropagation();
                                          _vm.show = "year";
                                        }
                                      }
                                    },
                                    [
                                      _c(
                                        "svg",
                                        {
                                          attrs: {
                                            width: "10px",
                                            height: "5px",
                                            viewBox: "0 0 10 5",
                                            version: "1.1",
                                            xmlns: "http://www.w3.org/2000/svg",
                                            "xmlns:xlink":
                                              "http://www.w3.org/1999/xlink"
                                          }
                                        },
                                        [
                                          _c("title", [
                                            _vm._v("icons/ic_arrow_drop_down")
                                          ]),
                                          _vm._v(" "),
                                          _c("desc", [
                                            _vm._v("Created with Sketch.")
                                          ]),
                                          _vm._v(" "),
                                          _c("defs"),
                                          _vm._v(" "),
                                          _c(
                                            "g",
                                            {
                                              attrs: {
                                                id: "Page-1",
                                                stroke: "none",
                                                "stroke-width": "1",
                                                fill: "none",
                                                "fill-rule": "evenodd"
                                              }
                                            },
                                            [
                                              _c(
                                                "g",
                                                {
                                                  attrs: {
                                                    id: "Artboard",
                                                    transform:
                                                      "translate(-283.000000, -34.000000)"
                                                  }
                                                },
                                                [
                                                  _c(
                                                    "g",
                                                    {
                                                      attrs: {
                                                        id: "Group-Copy",
                                                        transform:
                                                          "translate(231.000000, 24.000000)"
                                                      }
                                                    },
                                                    [
                                                      _c(
                                                        "g",
                                                        {
                                                          attrs: {
                                                            id: "Group-14",
                                                            transform:
                                                              "translate(45.000000, 0.000000)"
                                                          }
                                                        },
                                                        [
                                                          _c(
                                                            "g",
                                                            {
                                                              attrs: {
                                                                id:
                                                                  "icons/ic_arrow_drop_down"
                                                              }
                                                            },
                                                            [
                                                              _c(
                                                                "g",
                                                                {
                                                                  attrs: {
                                                                    id:
                                                                      "ic_arrow_drop_down"
                                                                  }
                                                                },
                                                                [
                                                                  _c(
                                                                    "g",
                                                                    {
                                                                      attrs: {
                                                                        id:
                                                                          "Icon-24px"
                                                                      }
                                                                    },
                                                                    [
                                                                      _c(
                                                                        "polygon",
                                                                        {
                                                                          attrs: {
                                                                            id:
                                                                              "Shape",
                                                                            "fill-opacity":
                                                                              "0.9",
                                                                            fill:
                                                                              "#FFFFFF",
                                                                            points:
                                                                              "7 10 12 15 17 10"
                                                                          }
                                                                        }
                                                                      ),
                                                                      _vm._v(" "),
                                                                      _c(
                                                                        "polygon",
                                                                        {
                                                                          attrs: {
                                                                            id:
                                                                              "Shape",
                                                                            points:
                                                                              "0 0 24 0 24 24 0 24"
                                                                          }
                                                                        }
                                                                      )
                                                                    ]
                                                                  )
                                                                ]
                                                              )
                                                            ]
                                                          )
                                                        ]
                                                      )
                                                    ]
                                                  )
                                                ]
                                              )
                                            ]
                                          )
                                        ]
                                      )
                                    ]
                                  )
                                ]),
                                _vm._v(" "),
                                _c(
                                  "div",
                                  {
                                    staticClass: "lb-vue-cal_arrow",
                                    on: {
                                      click: function($event) {
                                        $event.stopPropagation();
                                        return _vm.changeMonth(1)
                                      }
                                    }
                                  },
                                  [
                                    _c(
                                      "svg",
                                      {
                                        attrs: {
                                          width: "8px",
                                          height: "12px",
                                          viewBox: "0 0 8 12",
                                          version: "1.1",
                                          xmlns: "http://www.w3.org/2000/svg",
                                          "xmlns:xlink":
                                            "http://www.w3.org/1999/xlink"
                                        }
                                      },
                                      [
                                        _c("title", [
                                          _vm._v("icons/ic_chevron_right")
                                        ]),
                                        _vm._v(" "),
                                        _c("desc", [
                                          _vm._v("Created with Sketch.")
                                        ]),
                                        _vm._v(" "),
                                        _c("defs"),
                                        _vm._v(" "),
                                        _c(
                                          "g",
                                          {
                                            attrs: {
                                              id: "Page-1",
                                              stroke: "none",
                                              "stroke-width": "1",
                                              fill: "none",
                                              "fill-rule": "evenodd"
                                            }
                                          },
                                          [
                                            _c(
                                              "g",
                                              {
                                                attrs: {
                                                  id: "Artboard",
                                                  transform:
                                                    "translate(-360.000000, -30.000000)"
                                                }
                                              },
                                              [
                                                _c(
                                                  "g",
                                                  {
                                                    attrs: {
                                                      id: "Group-13",
                                                      transform:
                                                        "translate(352.000000, 24.000000)"
                                                    }
                                                  },
                                                  [
                                                    _c(
                                                      "g",
                                                      {
                                                        attrs: {
                                                          id:
                                                            "icons/ic_chevron_right"
                                                        }
                                                      },
                                                      [
                                                        _c(
                                                          "g",
                                                          {
                                                            attrs: {
                                                              id: "ic_chevron_right"
                                                            }
                                                          },
                                                          [
                                                            _c(
                                                              "g",
                                                              {
                                                                attrs: {
                                                                  id: "Icon-24px"
                                                                }
                                                              },
                                                              [
                                                                _c("polygon", {
                                                                  attrs: {
                                                                    id: "Shape",
                                                                    "fill-opacity":
                                                                      "0.9",
                                                                    fill: "#FFFFFF",
                                                                    points:
                                                                      "10 6 8.59 7.41 13.17 12 8.59 16.59 10 18 16 12"
                                                                  }
                                                                }),
                                                                _vm._v(" "),
                                                                _c("polygon", {
                                                                  attrs: {
                                                                    id: "Shape",
                                                                    points:
                                                                      "0 0 24 0 24 24 0 24"
                                                                  }
                                                                })
                                                              ]
                                                            )
                                                          ]
                                                        )
                                                      ]
                                                    )
                                                  ]
                                                )
                                              ]
                                            )
                                          ]
                                        )
                                      ]
                                    )
                                  ]
                                )
                              ]
                            ),
                            _vm._v(" "),
                            _c(
                              "div",
                              { staticClass: "inner-content" },
                              [
                                _c(
                                  "div",
                                  { staticClass: "lb-vue-cal_row days-name" },
                                  _vm._l(_vm.weekDays, function(day, index) {
                                    return _c(
                                      "div",
                                      { key: index, staticClass: "calendar-days" },
                                      [_vm._v(_vm._s(day.text))]
                                    )
                                  }),
                                  0
                                ),
                                _vm._v(" "),
                                _c("div", { staticClass: "clear10" }, [
                                  _vm._v(" ")
                                ]),
                                _vm._v(" "),
                                _vm._l(_vm.monthDays, function(row, index) {
                                  return _c(
                                    "div",
                                    {
                                      key: index,
                                      staticClass: "lb-vue-cal_row date-numbers"
                                    },
                                    _vm._l(row, function(day, index) {
                                      return _c(
                                        "div",
                                        {
                                          key: index,
                                          staticClass: "single-day",
                                          class:
                                            day.class +
                                            " " +
                                            (!day.active ? "disabled" : ""),
                                          on: {
                                            click: function($event) {
                                              $event.stopPropagation();
                                              return _vm.changeDate(day)
                                            }
                                          }
                                        },
                                        [_c("span", [_vm._v(_vm._s(day.text))])]
                                      )
                                    }),
                                    0
                                  )
                                }),
                                _vm._v(" "),
                                _c(
                                  "div",
                                  { staticClass: "lb-vue-cal_row button-row" },
                                  [
                                    _c(
                                      "button",
                                      {
                                        attrs: { disabled: !_vm.isActiveDay() },
                                        on: {
                                          click: function($event) {
                                            $event.stopPropagation();
                                            return _vm.setDate.apply(
                                              null,
                                              arguments
                                            )
                                          }
                                        }
                                      },
                                      [_vm._v(_vm._s(_vm.translate("Accept")))]
                                    )
                                  ]
                                )
                              ],
                              2
                            )
                          ])
                        : _vm._e(),
                      _vm._v(" "),
                      _vm.show == "month"
                        ? _c(
                            "div",
                            { staticClass: "lb-vue-cal_calendar change-month" },
                            [
                              _c(
                                "div",
                                { staticClass: "lb-vue-cal_row calendar-head" },
                                [_vm._v(_vm._s(_vm.translate("Select month")))]
                              ),
                              _vm._v(" "),
                              _c(
                                "div",
                                { staticClass: "inner-content" },
                                _vm._l(_vm.months, function(month, i) {
                                  return _c(
                                    "div",
                                    {
                                      key: i,
                                      staticClass: "lb-vue-cal_row single-month",
                                      class: {
                                        active: month.selected,
                                        disabled: !month.active
                                      }
                                    },
                                    [
                                      _c(
                                        "div",
                                        {
                                          on: {
                                            click: function($event) {
                                              $event.stopPropagation();
                                              _vm.changeDate(month);
                                              _vm.show = "date";
                                            }
                                          }
                                        },
                                        [_vm._v(_vm._s(month.text))]
                                      )
                                    ]
                                  )
                                }),
                                0
                              )
                            ]
                          )
                        : _vm._e(),
                      _vm._v(" "),
                      _vm.show == "year"
                        ? _c(
                            "div",
                            { staticClass: "lb-vue-cal_calendar change-year" },
                            [
                              _c(
                                "div",
                                { staticClass: "lb-vue-cal_row calendar-head" },
                                [_vm._v(_vm._s(_vm.translate("Select year")))]
                              ),
                              _vm._v(" "),
                              _c(
                                "div",
                                { staticClass: "inner-content" },
                                _vm._l(_vm.years, function(year, i) {
                                  return _c(
                                    "div",
                                    {
                                      key: i,
                                      ref: year.selected
                                        ? "lb_dtp_selected"
                                        : false,
                                      refInFor: true,
                                      staticClass: "lb-vue-cal_row single-year",
                                      class: {
                                        active: year.selected,
                                        disabled: !year.active
                                      }
                                    },
                                    [
                                      _c(
                                        "div",
                                        {
                                          on: {
                                            click: function($event) {
                                              $event.stopPropagation();
                                              _vm.changeDate(year);
                                              _vm.show = "date";
                                            }
                                          }
                                        },
                                        [_vm._v(_vm._s(year.text))]
                                      )
                                    ]
                                  )
                                }),
                                0
                              )
                            ]
                          )
                        : _vm._e(),
                      _vm._v(" "),
                      _vm.gettype == "time"
                        ? _c("div", { staticClass: "lb-vue-cal_calendar" }, [
                            _c(
                              "div",
                              { staticClass: "lb-vue-cal_row calendar-head" },
                              [_vm._v(_vm._s(_vm.translate("Select time")))]
                            ),
                            _vm._v(" "),
                            _c(
                              "div",
                              { staticClass: "inner-content content-time" },
                              [
                                _c(
                                  "div",
                                  { staticClass: "hours" },
                                  _vm._l(_vm.hours, function(hour, i) {
                                    return _c("div", { key: "h" + i }, [
                                      _c(
                                        "div",
                                        {
                                          ref: hour.selected
                                            ? "lb_dtp_selected"
                                            : false,
                                          refInFor: true,
                                          class: {
                                            active: hour.selected,
                                            disabled: !hour.active
                                          },
                                          on: {
                                            click: function($event) {
                                              $event.stopPropagation();
                                              return _vm.changeDate(hour)
                                            }
                                          }
                                        },
                                        [_vm._v(_vm._s(hour.text))]
                                      )
                                    ])
                                  }),
                                  0
                                ),
                                _vm._v(" "),
                                _c(
                                  "div",
                                  { staticClass: "minutes" },
                                  _vm._l(_vm.minutes, function(minute, i) {
                                    return _c("div", { key: "m" + i }, [
                                      _c(
                                        "div",
                                        {
                                          ref: minute.selected
                                            ? "lb_dtp_selected"
                                            : false,
                                          refInFor: true,
                                          class: {
                                            active: minute.selected,
                                            disabled: !minute.active
                                          },
                                          on: {
                                            click: function($event) {
                                              $event.stopPropagation();
                                              return _vm.changeDate(minute)
                                            }
                                          }
                                        },
                                        [_vm._v(_vm._s(minute.text))]
                                      )
                                    ])
                                  }),
                                  0
                                ),
                                _vm._v(" "),
                                _vm.showSeconds == null || _vm.showSeconds === true
                                  ? _c(
                                      "div",
                                      { staticClass: "seconds" },
                                      _vm._l(_vm.seconds, function(second, i) {
                                        return _c("div", { key: "s" + i }, [
                                          _c(
                                            "div",
                                            {
                                              ref: second.selected
                                                ? "lb_dtp_selected"
                                                : false,
                                              refInFor: true,
                                              class: {
                                                active: second.selected,
                                                disabled: !second.active
                                              },
                                              on: {
                                                click: function($event) {
                                                  $event.stopPropagation();
                                                  return _vm.changeDate(second)
                                                }
                                              }
                                            },
                                            [_vm._v(_vm._s(second.text))]
                                          )
                                        ])
                                      }),
                                      0
                                    )
                                  : _vm._e()
                              ]
                            ),
                            _vm._v(" "),
                            _c(
                              "div",
                              {
                                staticClass: "lb-vue-cal_row button-row button-time"
                              },
                              [
                                _c(
                                  "button",
                                  {
                                    on: {
                                      click: function($event) {
                                        $event.stopPropagation();
                                        return _vm.setDate.apply(null, arguments)
                                      }
                                    }
                                  },
                                  [_vm._v(_vm._s(_vm.translate("Accept")))]
                                )
                              ]
                            )
                          ])
                        : _vm._e()
                    ]
                  )
                ]
              )
            : _vm._e()
        ]
      )
    };
    var __vue_staticRenderFns__ = [];
    __vue_render__._withStripped = true;

      /* style */
      var __vue_inject_styles__ = function (inject) {
        if (!inject) { return }
        inject("data-v-435a2476_0", { source: ".lb-vue-cal_calendar[data-v-435a2476] {\n  all: initial;\n  font-family: 'Roboto', sans-serif;\n  position: relative;\n}\n.lb-vue-cal_calendar .lb-vue-cal_overlay[data-v-435a2476] {\n    position: fixed;\n    width: 100%;\n    height: 100%;\n    top: 0px;\n    left: 0px;\n    z-index: 10000;\n    background-color: rgba(0, 0, 0, 0.5);\n}\n.lb-vue-cal_calendar .simple_cal_cont[data-v-435a2476] {\n    position: fixed;\n    z-index: 2;\n}\n.lb-vue-cal_calendar .simple_cal[data-v-435a2476] {\n    display: inline-block;\n    background-color: #ffffff;\n}\n.lb-vue-cal_calendar .simple_cal .lb-vue-cal_calendar[data-v-435a2476] {\n      position: relative;\n      top: calc(100% + 5px);\n      left: 0px;\n      margin: 0px;\n      width: 400px;\n      display: block;\n}\n.lb-vue-cal_calendar .clear[data-v-435a2476] {\n    position: absolute;\n    right: 0;\n    top: 0;\n    width: 20px;\n    border: none;\n    outline: none;\n    background-color: #ffffff;\n    margin-right: 2px;\n    cursor: pointer;\n}\n.lb-vue-cal_calendar .lb-vue-cal_calendar[data-v-435a2476] {\n    position: fixed;\n    width: 400px;\n    top: calc((100% - 520px) / 2);\n    left: 50%;\n    z-index: 10001;\n    margin-left: -250px;\n    background-color: #ffffff;\n}\n@media only screen and (max-width: 37.5em) {\n.lb-vue-cal_calendar .lb-vue-cal_calendar[data-v-435a2476] {\n        left: 15%;\n        margin-left: 0;\n}\n}\n@media only screen and (max-width: 31.25em) {\n.lb-vue-cal_calendar .lb-vue-cal_calendar[data-v-435a2476] {\n        left: 10%;\n}\n}\n@media only screen and (max-width: 28.75em) {\n.lb-vue-cal_calendar .lb-vue-cal_calendar[data-v-435a2476] {\n        left: 6%;\n}\n}\n@media only screen and (max-width: 25em) {\n.lb-vue-cal_calendar .lb-vue-cal_calendar[data-v-435a2476] {\n        left: 2.5%;\n        width: 95%;\n        margin: auto;\n        display: block;\n        text-align: center;\n        align-items: center;\n}\n}\n.lb-vue-cal_calendar .lb-vue-cal_calendar .lb-vue-cal_row[data-v-435a2476] {\n      display: flex;\n}\n.lb-vue-cal_calendar .lb-vue-cal_calendar .lb-vue-cal_row .lb-vue-cal_arrow[data-v-435a2476] {\n        cursor: pointer;\n}\n.lb-vue-cal_calendar .lb-vue-cal_calendar .lb-vue-cal_row .active[data-v-435a2476] {\n        background-color: #004C65;\n        color: rgba(255, 255, 255, 0.9);\n        border-radius: 6px;\n}\n.lb-vue-cal_calendar .calendar-head[data-v-435a2476] {\n    background-color: #004C65;\n    height: 3rem;\n    color: rgba(255, 255, 255, 0.9);\n    align-items: center;\n    justify-content: space-between;\n    padding: 0 1rem;\n}\n.lb-vue-cal_calendar .calendar-head .month[data-v-435a2476] {\n      cursor: pointer;\n}\n.lb-vue-cal_calendar .calendar-head .month div[data-v-435a2476] {\n        float: left;\n}\n.lb-vue-cal_calendar .calendar-head .drop-down-arrow[data-v-435a2476] {\n      margin-left: .5rem;\n}\n.lb-vue-cal_calendar .calendar-head .drop-down-arrow img[data-v-435a2476] {\n        margin-bottom: .1rem;\n}\n.lb-vue-cal_calendar .calendar-head .year[data-v-435a2476] {\n      cursor: pointer;\n}\n.lb-vue-cal_calendar .calendar-head .year div[data-v-435a2476] {\n        float: left;\n}\n.lb-vue-cal_calendar .inner-content[data-v-435a2476] {\n    padding: 0 1rem;\n    color: #004C65;\n}\n.lb-vue-cal_calendar .inner-content .days-name[data-v-435a2476] {\n      justify-content: space-evenly;\n      padding: 1.5rem 0;\n      font-weight: 400;\n      border-bottom: 1px solid rgba(185, 185, 185, 0.25);\n}\n.lb-vue-cal_calendar .inner-content .days-name .calendar-days[data-v-435a2476] {\n        width: 14.28571%;\n        display: inline-block;\n        text-align: center;\n}\n.lb-vue-cal_calendar .inner-content .date-numbers[data-v-435a2476] {\n      font-weight: 600;\n}\n.lb-vue-cal_calendar .inner-content .date-numbers .single-day[data-v-435a2476] {\n        display: inline-block;\n        width: 14.28571%;\n        line-height: 3rem;\n        text-align: center;\n        cursor: pointer;\n        font-size: 1.2rem;\n        font-weight: 300;\n        margin: .2rem 0;\n}\n.lb-vue-cal_calendar .inner-content .date-numbers .single-day.disabled[data-v-435a2476] {\n          opacity: .3;\n}\n.lb-vue-cal_calendar .inner-content .date-numbers .single-day[data-v-435a2476]:hover {\n          background-color: #004C65;\n          color: rgba(255, 255, 255, 0.9);\n          transition: .3s;\n          border-radius: 6px;\n}\n.lb-vue-cal_calendar .inner-content .date-numbers .previous-month[data-v-435a2476] {\n        opacity: .3;\n        font-weight: 300;\n}\n.lb-vue-cal_calendar .inner-content .date-numbers .next-month[data-v-435a2476] {\n        opacity: .3;\n        font-weight: 300;\n}\n.lb-vue-cal_calendar .button-row[data-v-435a2476] {\n    display: block !important;\n}\n.lb-vue-cal_calendar .button-row button[data-v-435a2476] {\n      float: right;\n      padding: .5rem 2rem;\n      border-radius: 4px;\n      background-color: #F7DB6C;\n      border: 0;\n      color: rgba(0, 0, 0, 0.8);\n      font-weight: 600;\n      font-size: .8rem;\n      margin-top: 1.5rem;\n      margin-bottom: 1.5rem;\n      cursor: pointer;\n}\n.lb-vue-cal_calendar .button-row button[data-v-435a2476]:hover {\n        background-color: #f5de82;\n        transition: .3s;\n}\n.lb-vue-cal_calendar .button-row button[data-v-435a2476]:disabled {\n        pointer-events: none;\n        opacity: .65;\n}\n.lb-vue-cal_calendar .change-month .calendar-head[data-v-435a2476] {\n    margin-bottom: 1rem;\n}\n.lb-vue-cal_calendar .change-month .single-month[data-v-435a2476] {\n    display: block;\n    width: 48%;\n    float: left;\n    text-align: center;\n    border: 1px solid rgba(185, 185, 185, 0.25);\n    margin-right: 1%;\n    padding: 1rem 0;\n    margin-bottom: .5rem;\n    border-radius: 6px;\n    color: #004C65;\n    text-transform: capitalize;\n    cursor: pointer;\n    font-weight: 300;\n    font-size: 1.5rem;\n}\n.lb-vue-cal_calendar .change-month .single-month.disabled[data-v-435a2476] {\n      opacity: .3;\n}\n.lb-vue-cal_calendar .change-month .single-month[data-v-435a2476]:hover, .lb-vue-cal_calendar .change-month .single-month.active[data-v-435a2476] {\n      background-color: #004C65;\n      color: rgba(255, 255, 255, 0.9);\n      transition: .3s;\n}\n.lb-vue-cal_calendar .change-year[data-v-435a2476] {\n    height: 500px;\n}\n.lb-vue-cal_calendar .change-year .inner-content[data-v-435a2476] {\n      overflow-y: auto;\n      height: 85%;\n      margin-top: 1rem;\n}\n.lb-vue-cal_calendar .change-year .single-year[data-v-435a2476] {\n      display: block;\n      text-align: center;\n      margin-right: 1%;\n      padding: 1rem 0;\n      margin-bottom: .5rem;\n      border-radius: 6px;\n      color: #004C65;\n      text-transform: capitalize;\n      cursor: pointer;\n      font-weight: 300;\n      font-size: 1.5rem;\n}\n.lb-vue-cal_calendar .change-year .single-year.disabled[data-v-435a2476] {\n        opacity: .3;\n}\n.lb-vue-cal_calendar .change-year .single-year[data-v-435a2476]:hover {\n        font-weight: 600;\n        transition: .1s;\n        font-size: 1.7rem;\n}\n.lb-vue-cal_calendar .change-year .single-year.active[data-v-435a2476] {\n        background-color: #004C65;\n        color: rgba(255, 255, 255, 0.9);\n}\n.lb-vue-cal_calendar .content-time[data-v-435a2476] {\n    display: flex;\n    justify-content: space-evenly;\n    height: 75%;\n    margin-top: 1rem;\n    height: 350px;\n}\n.lb-vue-cal_calendar .content-time .hours[data-v-435a2476], .lb-vue-cal_calendar .content-time .minutes[data-v-435a2476], .lb-vue-cal_calendar .content-time .seconds[data-v-435a2476] {\n      overflow-y: auto;\n      width: 5rem;\n      text-align: center;\n      cursor: pointer;\n}\n.lb-vue-cal_calendar .content-time .hours div[data-v-435a2476], .lb-vue-cal_calendar .content-time .minutes div[data-v-435a2476], .lb-vue-cal_calendar .content-time .seconds div[data-v-435a2476] {\n        padding: .5rem 0;\n        font-weight: 300;\n        font-size: 1.5rem;\n        margin-right: .5rem;\n}\n.lb-vue-cal_calendar .content-time .hours div .active[data-v-435a2476], .lb-vue-cal_calendar .content-time .minutes div .active[data-v-435a2476], .lb-vue-cal_calendar .content-time .seconds div .active[data-v-435a2476] {\n          background-color: #004C65;\n          border-radius: 6px;\n          color: rgba(255, 255, 255, 0.9);\n}\n.lb-vue-cal_calendar .content-time .hours div[data-v-435a2476]:hover, .lb-vue-cal_calendar .content-time .minutes div[data-v-435a2476]:hover, .lb-vue-cal_calendar .content-time .seconds div[data-v-435a2476]:hover {\n          font-weight: 600;\n          transition: .1s;\n          font-size: 1.7rem;\n}\n.lb-vue-cal_calendar .button-time[data-v-435a2476] {\n    margin-right: 1rem;\n}\n.clear10[data-v-435a2476] {\n  display: block;\n  height: 10px;\n}\n[data-v-435a2476]::-webkit-scrollbar {\n  width: 8px;\n}\n\n/* Track */\n[data-v-435a2476]::-webkit-scrollbar-track {\n  background: #efefef;\n  border-radius: 10px;\n}\n\n/* Handle */\n[data-v-435a2476]::-webkit-scrollbar-thumb {\n  background: #F7DB6C;\n  border-radius: 10px;\n}\n\n/* Handle on hover */\n[data-v-435a2476]::-webkit-scrollbar-thumb:hover {\n  background: #004C65;\n  transition: .3s;\n}\n\n/*# sourceMappingURL=DateTimePicker.vue.map */", map: {"version":3,"sources":["/home/user/work_leapbit/lb-vue-datetimepicker/src/DateTimePicker.vue","DateTimePicker.vue"],"names":[],"mappings":"AAs9BA;EACA,YAAA;EACA,iCAAA;EACA,kBAAA;AAAA;AAHA;IAMA,eAAA;IACA,WAAA;IACA,YAAA;IACA,QAAA;IACA,SAAA;IACA,cAAA;IACA,oCAAA;AAAA;AAZA;IAeA,eAAA;IACA,UAAA;AAAA;AAhBA;IAoBA,qBAAA;IACA,yBAAA;AAAA;AArBA;MAyBA,kBAAA;MACA,qBAAA;MACA,SAAA;MACA,WAAA;MACA,YAAA;MACA,cAAA;AAAA;AA9BA;IAkCA,kBAAA;IACA,QAAA;IACA,MAAA;IACA,WAAA;IACA,YAAA;IACA,aAAA;IACA,yBAAA;IACA,iBAAA;IACA,eAAA;AAAA;AA1CA;IA8CA,eAAA;IACA,YAAA;IAEA,6BAAA;IACA,SAAA;IACA,cAAA;IACA,mBAAA;IACA,yBAAA;AAAA;AA/EA;AA0BA;QAwDA,SAAA;QACA,cAAA;AAAA;AAkCA;AArHA;AA0BA;QA6DA,SAAA;AAAA;AA8BA;AArHA;AA0BA;QAiEA,QAAA;AAAA;AA0BA;AArHA;AA0BA;QAqEA,UAAA;QACA,UAAA;QACA,YAAA;QACA,cAAA;QACA,kBAAA;QACA,mBAAA;AAAA;AAiBA;AA3FA;MA8EA,aAAA;AAAA;AA9EA;QAiFA,eAAA;AAAA;AAjFA;QAqFA,yBArHA;QAsHA,+BApHA;QAqHA,kBAAA;AAAA;AAvFA;IA8FA,yBA9HA;IA+HA,YAAA;IACA,+BA9HA;IA+HA,mBAAA;IACA,8BAAA;IACA,eAAA;AAAA;AAnGA;MAuGA,eAAA;AAAA;AAvGA;QA0GA,WAAA;AAAA;AA1GA;MA+GA,kBAAA;AAAA;AA/GA;QAkHA,oBAAA;AAAA;AAlHA;MAwHA,eAAA;AAAA;AAxHA;QA2HA,WAAA;AAAA;AA3HA;IAiIA,eAAA;IACA,cAlKA;AAAA;AAgCA;MAqIA,6BAAA;MACA,iBAAA;MACA,gBAAA;MACA,kDAAA;AAAA;AAxIA;QA2IA,gBAAA;QACA,qBAAA;QACA,kBAAA;AAAA;AA7IA;MAkJA,gBAAA;AAAA;AAlJA;QAqJA,qBAAA;QACA,gBAAA;QACA,iBAAA;QACA,kBAAA;QACA,eAAA;QACA,iBAAA;QACA,gBAAA;QACA,eAAA;AAAA;AA5JA;UA+JA,WAAA;AAAA;AA/JA;UAmKA,yBAnMA;UAoMA,+BAlMA;UAmMA,eAAA;UACA,kBAAA;AAAA;AAtKA;QA2KA,WAAA;QACA,gBAAA;AAAA;AA5KA;QAgLA,WAAA;QACA,gBAAA;AAAA;AAjLA;IAuLA,yBAAA;AAAA;AAvLA;MA0LA,YAAA;MACA,mBAAA;MACA,kBAAA;MACA,yBA5NA;MA6NA,SAAA;MACA,yBAAA;MACA,gBAAA;MACA,gBAAA;MACA,kBAAA;MACA,qBAAA;MACA,eAAA;AAAA;AApMA;QAuMA,yBAAA;QACA,eAAA;AAAA;AAxMA;QA2MA,oBAAA;QACA,YAAA;AAAA;AA5MA;IAoNA,mBAAA;AAAA;AApNA;IAwNA,cAAA;IACA,UAAA;IACA,WAAA;IACA,kBAAA;IACA,2CAAA;IACA,gBAAA;IACA,eAAA;IACA,oBAAA;IACA,kBAAA;IACA,cAjQA;IAkQA,0BAAA;IACA,eAAA;IACA,gBAAA;IACA,iBAAA;AAAA;AArOA;MAwOA,WAAA;AAAA;AAxOA;MA4OA,yBA5QA;MA6QA,+BA3QA;MA4QA,eAAA;AAAA;AA9OA;IAoPA,aAAA;AAAA;AApPA;MAsPA,gBAAA;MACA,WAAA;MACA,gBAAA;AAAA;AAxPA;MA4PA,cAAA;MAGA,kBAAA;MAEA,gBAAA;MACA,eAAA;MACA,oBAAA;MACA,kBAAA;MACA,cArSA;MAsSA,0BAAA;MACA,eAAA;MACA,gBAAA;MACA,iBAAA;AAAA;AAzQA;QA4QA,WAAA;AAAA;AA5QA;QAgRA,gBAAA;QACA,eAAA;QACA,iBAAA;AAAA;AAlRA;QAsRA,yBAtTA;QAuTA,+BArTA;AAAA;AA8BA;IA6RA,aAAA;IACA,6BAAA;IACA,WAAA;IACA,gBAAA;IACA,aAAA;AAAA;AAjSA;MAoSA,gBAAA;MACA,WAAA;MACA,kBAAA;MACA,eAAA;AAAA;AAvSA;QA0SA,gBAAA;QACA,gBAAA;QACA,iBAAA;QACA,mBAAA;AAAA;AA7SA;UAgTA,yBAhVA;UAiVA,kBAAA;UACA,+BAhVA;AAAA;AA8BA;UAsTA,gBAAA;UACA,eAAA;UACA,iBAAA;AAAA;AAxTA;IA+TA,kBAAA;AAAA;AAIA;EACA,cAAA;EACA,YAAA;AAAA;AAGA;EACA,UAAA;AAAA;;AAGA,UAAA;AACA;EACA,mBAAA;EACA,mBAAA;AAAA;;AAGA,WAAA;AACA;EACA,mBAnXA;EAoXA,mBAAA;AAAA;;AAGA,oBAAA;AACA;EACA,mBA1XA;EA2XA,eAAA;AAAA;;AC9jCA,6CAA6C","file":"DateTimePicker.vue","sourcesContent":["<template>\n    <div class=\"lb-vue-cal_calendar\" ref=\"lb-vue-cal_calendar\" tabindex=\"0\" @keydown.esc=\"close\">\n        <input type=\"text\" v-model=\"displayValue\" ref=\"lb_input\" :disabled=\"disabled\" :class=\"inputClass\" v-on:focus.stop=\"open\" />\n        <button class=\"clear\" v-if=\"disabled !== true && isClearable && value !== null && value !== undefined\" :disabled=\"disabled\" v-on:click=\"clearPicker\">X</button>\n        <div v-if=\"showPicker\" :class=\"{ 'simple_cal_cont': simple }\" ref=\"lb-vue-cal_cont\" :style=\"{ 'top':top, 'left':left, 'right': right,'height':height+'px'}\">\n            <div v-if=\"!simple\" class=\"lb-vue-cal_overlay\" v-on:click=\"close\"></div>\n            <div :class=\"{ 'simple_cal': simple }\" ref=\"lb-vue-cal_calview\">\n                <div class=\"lb-vue-cal_calendar\" v-if=\"show == 'date' && (gettype == 'datetime' || gettype == 'date')\" >\n                    <div class=\"lb-vue-cal_row calendar-head\">\n                        <div class=\"lb-vue-cal_arrow\" v-on:click.stop=\"changeMonth(-1)\"><svg width=\"8px\" height=\"12px\" viewBox=\"0 0 8 12\" version=\"1.1\" xmlns=\"http://www.w3.org/2000/svg\" xmlns:xlink=\"http://www.w3.org/1999/xlink\">\n                                <!-- Generator: Sketch 51.3 (57544) - http://www.bohemiancoding.com/sketch -->\n                                <title>Group 11</title>\n                                <desc>Created with Sketch.</desc>\n                                <defs></defs>\n                                <g id=\"Page-1\" stroke=\"none\" stroke-width=\"1\" fill=\"none\" fill-rule=\"evenodd\">\n                                    <g id=\"Artboard\" transform=\"translate(-32.000000, -30.000000)\">\n                                        <g id=\"Group-12\" transform=\"translate(24.000000, 24.000000)\">\n                                            <g id=\"Group-11\">\n                                                <g id=\"icons/ic_chevron_left\">\n                                                    <g id=\"ic_chevron_left\">\n                                                        <g id=\"Icon-24px\">\n                                                            <polygon id=\"Shape\" fill-opacity=\"0.9\" fill=\"#FFFFFF\" points=\"15.41 7.41 14 6 8 12 14 18 15.41 16.59 10.83 12\"></polygon>\n                                                            <polygon id=\"Shape\" points=\"0 0 24 0 24 24 0 24\"></polygon>\n                                                        </g>\n                                                    </g>\n                                                </g>\n                                            </g>\n                                        </g>\n                                    </g>\n                                </g>\n                            </svg></div>\n                        <div class=\"month\">\n                            <div v-on:click.stop=\"show = 'month'\">{{month}}</div>\n                            <div class=\"lb-vue-cal_arrow drop-down-arrow\" v-on:click.stop=\"show = 'month'\"><svg width=\"10px\" height=\"5px\" viewBox=\"0 0 10 5\" version=\"1.1\" xmlns=\"http://www.w3.org/2000/svg\" xmlns:xlink=\"http://www.w3.org/1999/xlink\">\n                                    <!-- Generator: Sketch 51.3 (57544) - http://www.bohemiancoding.com/sketch -->\n                                    <title>icons/ic_arrow_drop_down</title>\n                                    <desc>Created with Sketch.</desc>\n                                    <defs></defs>\n                                    <g id=\"Page-1\" stroke=\"none\" stroke-width=\"1\" fill=\"none\" fill-rule=\"evenodd\">\n                                        <g id=\"Artboard\" transform=\"translate(-283.000000, -34.000000)\">\n                                            <g id=\"Group-Copy\" transform=\"translate(231.000000, 24.000000)\">\n                                                <g id=\"Group-14\" transform=\"translate(45.000000, 0.000000)\">\n                                                    <g id=\"icons/ic_arrow_drop_down\">\n                                                        <g id=\"ic_arrow_drop_down\">\n                                                            <g id=\"Icon-24px\">\n                                                                <polygon id=\"Shape\" fill-opacity=\"0.9\" fill=\"#FFFFFF\" points=\"7 10 12 15 17 10\"></polygon>\n                                                                <polygon id=\"Shape\" points=\"0 0 24 0 24 24 0 24\"></polygon>\n                                                            </g>\n                                                        </g>\n                                                    </g>\n                                                </g>\n                                            </g>\n                                        </g>\n                                    </g>\n                                </svg></div>\n                        </div>\n                        <div class=\"year\">\n                            <div v-on:click.stop=\"show = 'year'\">{{year}}</div>\n                            <div class=\"lb-vue-cal_arrow drop-down-arrow\" v-on:click.stop=\"show = 'year'\"><svg width=\"10px\" height=\"5px\" viewBox=\"0 0 10 5\" version=\"1.1\" xmlns=\"http://www.w3.org/2000/svg\" xmlns:xlink=\"http://www.w3.org/1999/xlink\">\n                                    <!-- Generator: Sketch 51.3 (57544) - http://www.bohemiancoding.com/sketch -->\n                                    <title>icons/ic_arrow_drop_down</title>\n                                    <desc>Created with Sketch.</desc>\n                                    <defs></defs>\n                                    <g id=\"Page-1\" stroke=\"none\" stroke-width=\"1\" fill=\"none\" fill-rule=\"evenodd\">\n                                        <g id=\"Artboard\" transform=\"translate(-283.000000, -34.000000)\">\n                                            <g id=\"Group-Copy\" transform=\"translate(231.000000, 24.000000)\">\n                                                <g id=\"Group-14\" transform=\"translate(45.000000, 0.000000)\">\n                                                    <g id=\"icons/ic_arrow_drop_down\">\n                                                        <g id=\"ic_arrow_drop_down\">\n                                                            <g id=\"Icon-24px\">\n                                                                <polygon id=\"Shape\" fill-opacity=\"0.9\" fill=\"#FFFFFF\" points=\"7 10 12 15 17 10\"></polygon>\n                                                                <polygon id=\"Shape\" points=\"0 0 24 0 24 24 0 24\"></polygon>\n                                                            </g>\n                                                        </g>\n                                                    </g>\n                                                </g>\n                                            </g>\n                                        </g>\n                                    </g>\n                                </svg></div>\n                        </div>\n                        <div class=\"lb-vue-cal_arrow\" v-on:click.stop=\"changeMonth(1)\"><svg width=\"8px\" height=\"12px\" viewBox=\"0 0 8 12\" version=\"1.1\" xmlns=\"http://www.w3.org/2000/svg\" xmlns:xlink=\"http://www.w3.org/1999/xlink\">\n                                <!-- Generator: Sketch 51.3 (57544) - http://www.bohemiancoding.com/sketch -->\n                                <title>icons/ic_chevron_right</title>\n                                <desc>Created with Sketch.</desc>\n                                <defs></defs>\n                                <g id=\"Page-1\" stroke=\"none\" stroke-width=\"1\" fill=\"none\" fill-rule=\"evenodd\">\n                                    <g id=\"Artboard\" transform=\"translate(-360.000000, -30.000000)\">\n                                        <g id=\"Group-13\" transform=\"translate(352.000000, 24.000000)\">\n                                            <g id=\"icons/ic_chevron_right\">\n                                                <g id=\"ic_chevron_right\">\n                                                    <g id=\"Icon-24px\">\n                                                        <polygon id=\"Shape\" fill-opacity=\"0.9\" fill=\"#FFFFFF\" points=\"10 6 8.59 7.41 13.17 12 8.59 16.59 10 18 16 12\"></polygon>\n                                                        <polygon id=\"Shape\" points=\"0 0 24 0 24 24 0 24\"></polygon>\n                                                    </g>\n                                                </g>\n                                            </g>\n                                        </g>\n                                    </g>\n                                </g>\n                            </svg></div>\n                    </div>\n                    <div class=\"inner-content\">\n                        <div class=\"lb-vue-cal_row days-name\">\n                            <div v-for=\"(day, index) in weekDays\" :key=\"index\" class=\"calendar-days\">{{day.text}}</div>\n                        </div>\n                        <div class=\"clear10\">&nbsp;</div>\n                        <div class=\"lb-vue-cal_row date-numbers\" v-for=\"(row, index) in monthDays\" :key=\"index\">\n                            <div class=\"single-day\" v-for=\"(day, index) in row\"  :class=\"day.class+' '+(!day.active ? 'disabled' : '')\"  v-on:click.stop=\"changeDate(day)\" :key=\"index\">\n                                <span>{{day.text}}</span>\n                            </div>\n                        </div>\n                        <div class=\"lb-vue-cal_row button-row\">\n                            <button v-on:click.stop=\"setDate\"\n                            :disabled=\"!isActiveDay()\">{{translate('Accept')}}</button>\n                        </div>\n                    </div>\n                </div>\n                <div class=\"lb-vue-cal_calendar change-month\" v-if=\"show == 'month'\">\n                    <div class=\"lb-vue-cal_row calendar-head\">{{translate('Select month')}}</div>\n                    <div class=\"inner-content\">\n                        <div class=\"lb-vue-cal_row single-month\" v-for=\"(month, i) in months\" :key=\"i\" :class=\"{'active': month.selected, 'disabled': !month.active }\">\n                            <div v-on:click.stop=\"changeDate(month); show = 'date';\">{{month.text}}</div>\n                        </div>\n                    </div>\n                </div>\n                <div class=\"lb-vue-cal_calendar change-year\" v-if=\"show == 'year'\">\n                    <div class=\"lb-vue-cal_row calendar-head\">{{translate('Select year')}}</div>\n                    <div class=\"inner-content\">\n                        <div class=\"lb-vue-cal_row single-year\" v-for=\"(year, i) in years\" :key=\"i\" :ref=\"(year.selected) ? 'lb_dtp_selected' : false\" :class=\"{'active': year.selected, 'disabled': !year.active }\">\n                            <div v-on:click.stop=\"changeDate(year); show = 'date';\">{{year.text}}</div>\n                        </div>\n                    </div>\n                </div>\n                <div class=\"lb-vue-cal_calendar\" v-if=\"gettype == 'time'\">\n                    <div class=\"lb-vue-cal_row calendar-head\">{{translate('Select time')}}</div>\n                    <div class=\"inner-content content-time\">\n                        <div class=\"hours\">\n                            <div v-for=\"(hour, i) in hours\" :key=\"'h'+i\">\n                                <div\n                                :ref=\"(hour.selected) ? 'lb_dtp_selected' : false\"\n                                v-on:click.stop=\"changeDate(hour)\"\n                                :class=\"{'active': hour.selected, 'disabled': !hour.active }\">{{hour.text}}</div>\n                            </div>\n                        </div>\n                        <div class=\"minutes\">\n                            <div v-for=\"(minute, i) in minutes\" :key=\"'m'+i\">\n                                <div\n                                :ref=\"(minute.selected) ? 'lb_dtp_selected' : false\"\n                                v-on:click.stop=\"changeDate(minute)\"\n                                :class=\"{'active': minute.selected, 'disabled': !minute.active }\">{{minute.text}}</div>\n                            </div>\n                        </div>\n                        <div class=\"seconds\" v-if=\"showSeconds == null || showSeconds === true\">\n                            <div v-for=\"(second, i) in seconds\" :key=\"'s'+i\">\n                                <div\n                                :ref=\"(second.selected) ? 'lb_dtp_selected' : false\"\n                                v-on:click.stop=\"changeDate(second)\"\n                                :class=\"{'active': second.selected, 'disabled': !second.active }\">{{second.text}}</div>\n                            </div>\n                        </div>\n                    </div>\n                    <div class=\"lb-vue-cal_row button-row button-time\">\n                        <button v-on:click.stop=\"setDate\">{{translate('Accept')}}</button>\n                    </div>\n                </div>\n            </div>\n        </div>\n    </div>\n</template>\n\n<script>\n\nimport moment from 'moment';\n\nexport default\n{\n    name: 'DateTimePicker',\n    props: [\n        'value',\n        'inputClass',\n        'translateMethod',\n        'type',\n        'format',\n        'displayFormat',\n        'minYear',\n        'maxYear',\n        'simple',\n        'isUTC',\n        'start',\n        'end',\n        'isClearable',\n        'disabled',\n        'showSeconds',\n    ],\n    data()\n    {\n        const me = this;\n        return {\n            gettype: '',\n            show: '',\n            calcDate: null,\n            showPicker: false,\n            minYearDef: 1900,\n            maxYearDef: 2100,\n            startCalDate: null,\n            endCalDate: null,\n            isStart: null,\n            isEnd: null,\n            top: '',\n            left: '',\n            right: '',\n            height: '',\n            weekDays: [\n                {\n                    key: '1',\n                    text: me.translate('Mon'),\n                },\n                {\n                    key: '2',\n                    text: me.translate('Tue'),\n                },\n                {\n                    key: '3',\n                    text: me.translate('Wed'),\n                },\n                {\n                    key: '4',\n                    text: me.translate('Thu'),\n                },\n                {\n                    key: '5',\n                    text: me.translate('Fri'),\n                },\n                {\n                    key: '6',\n                    text: me.translate('Sat'),\n                },\n                {\n                    key: '0',\n                    text: me.translate('Sun'),\n                }],\n            monthDays: [],\n        };\n    },\n    created()\n    {\n        const me = this;\n\n        me.setValue();\n\n        me.setStartEndValues();\n\n        if (me.type === undefined)\n        {\n            me.gettype = 'datetime';\n        }\n        else\n        {\n            me.gettype = me.type;\n        }\n\n        if (me.minYear !== undefined)\n        {\n            me.minYearDef = parseInt(me.minYear, 10);\n        }\n        if (me.maxYear !== undefined)\n        {\n            me.maxYearDef = parseInt(me.maxYear, 10);\n        }\n\n        window.addEventListener('mousedown', me.windowEvt);\n        window.addEventListener('scroll', me.scrollHandler);\n        window.addEventListener('resize', me.reposition);\n    },\n    destroyed()\n    {\n        const me = this;\n\n        window.removeEventListener('mousedown', me.windowEvt);\n        window.removeEventListener('scroll', me.scrollHandler);\n        window.removeEventListener('resize', me.reposition);\n    },\n    watch:\n    {\n        value()\n        {\n            const me = this;\n            me.setValue();\n            me.calculateCalendar();\n        },\n        calcDate:\n        {\n            handler()\n            {\n                const me = this;\n                me.calculateCalendar();\n            },\n            deep: true,\n        },\n        gettype(val)\n        {\n            const me = this;\n\n            if (val === 'time')\n            {\n                me.centerValues();\n            }\n        },\n        show(val)\n        {\n            const me = this;\n\n            if (val === 'year')\n            {\n                me.centerValues();\n            }\n        },\n        showPicker(val)\n        {\n            const me = this;\n\n            if (val)\n            {\n                me.show = 'date';\n                if (me.type === undefined)\n                {\n                    me.gettype = 'datetime';\n                }\n                else\n                {\n                    me.gettype = me.type;\n                }\n\n                me.$nextTick(() =>\n                {\n                    me.scrollHandler();\n                });\n            }\n        },\n        start()\n        {\n            const me = this;\n            me.setStartEndValues();\n        },\n        end()\n        {\n            const me = this;\n            me.setStartEndValues();\n        },\n    },\n    methods:\n    {\n        isActiveDay()\n        {\n            const me = this;\n            for (const week of me.monthDays)\n            {\n                for (const day of week)\n                {\n                    if (day.active === true && day.date.format('YYYY-MM-DD') === me.calcDate.format('YYYY-MM-DD'))\n                    {\n                        return true;\n                    }\n                }\n            }\n            return false;\n        },\n        setStartEndValues()\n        {\n            const me = this;\n\n            if (me.start !== undefined)\n            {\n                me.startCalDate = me.parseValue(me.start);\n                me.isEnd = true;\n            }\n\n            if (me.end !== undefined)\n            {\n                me.endCalDate = me.parseValue(me.end);\n                me.isStart = true;\n            }\n\n            me.calculateCalendar();\n        },\n        setValue()\n        {\n            const me = this;\n\n            me.calcDate = me.parseValue(me.value);\n        },\n        parseValue(value)\n        {\n            const me = this;\n            if (value == null || value === undefined)\n            {\n                return moment();\n            }\n\n            if (me.format)\n            {\n                if (me.isUTC)\n                {\n                    return moment.utc(value, me.format).local();\n                }\n\n                return moment(value, me.format);\n            }\n\n            if (me.isUTC)\n            {\n                return moment.utc(value).local();\n            }\n\n            const val = moment(value);\n\n            if (!val.isValid())\n            {\n                return moment();\n            }\n\n            return moment();\n        },\n        windowEvt(evt)\n        {\n            const me = this;\n\n            if (me.showPicker === true)\n            {\n                if (me.$refs.lb_input === evt.target)\n                {\n                    return;\n                }\n                \n                const [clickedElement] = evt.composedPath();\n\n                if (clickedElement\n                && me.recursChildNodes(me.$refs['lb-vue-cal_calendar'].childNodes, clickedElement))\n                {\n                    return;\n                }\n\n                me.close();\n            }\n        },\n        scrollHandler()\n        {\n            const me = this;\n            let ticking = false;\n            let offset = 0;\n            if (!ticking)\n            {\n                window.requestAnimationFrame(() =>\n                {\n                    if (me.$refs['lb-vue-cal_cont'] !== undefined)\n                    {\n                        const calCont = me.$refs['lb-vue-cal_cont'];\n\n                        const target = me.$refs.lb_input;\n\n                        if (target.getBoundingClientRect().bottom + calCont.clientHeight > window.innerHeight)\n                        {\n                            offset = -calCont.clientHeight + -target.getBoundingClientRect().height;\n                        }\n\n                        const inputElementBottomPosition = me.$refs.lb_input.getBoundingClientRect().bottom;\n                        me.top = `${inputElementBottomPosition + offset}px`;\n\n                        ticking = false;\n                    }\n                });\n\n                ticking = true;\n            }\n        },\n        recursChildNodes(childNodes, target)\n        {\n            const me = this;\n            for (const node of childNodes)\n            {\n                if (node === target)\n                {\n                    return true;\n                }\n\n                if (me.recursChildNodes(node.childNodes, target))\n                {\n                    return true;\n                }\n            }\n            return false;\n        },\n        centerValues()\n        {\n            const me = this;\n            me.$nextTick(() =>\n            {\n                if (me.$refs.lb_dtp_selected)\n                {\n                    for (const ref of me.$refs.lb_dtp_selected)\n                    {\n                        ref.scrollIntoView({ block: 'center' });\n                    }\n                }\n            });\n        },\n        close()\n        {\n            const me = this;\n\n            if (me.showPicker === true)\n            {\n                me.showPicker = false;\n                if (!me.simple)\n                {\n                    me.$refs.lb_input.blur();\n                }\n            }\n        },\n        open()\n        {\n            const me = this;\n            me.reposition();\n            me.showPicker = true;\n        },\n        reposition()\n        {\n            const me = this;\n            const simpleCalendarWidth = 400;\n            const target = me.$refs.lb_input;\n\n            if (target.getBoundingClientRect().left + simpleCalendarWidth > window.innerWidth)\n            {\n                me.right = '0px';\n                me.left = '';\n            }\n            else\n            {\n                me.right = '';\n                me.left = `${target.getBoundingClientRect().left}px`;\n            }\n        },\n        changeMonth(by)\n        {\n            const me = this;\n            me.calcDate = moment(me.calcDate.add(by, 'month'));\n        },\n        translate(val)\n        {\n            const me = this;\n\n            if (me.translateMethod !== undefined)\n            {\n                return me.translateMethod(val);\n            }\n            return val;\n        },\n        setDate()\n        {\n            const me = this;\n\n            if (me.gettype === 'datetime')\n            {\n                me.gettype = 'time';\n            }\n            else if (me.gettype === 'date' || me.gettype === 'time')\n            {\n                me.sendDate();\n            }\n        },\n        getCalcDate()\n        {\n            const me = this;\n            return me.calcDate;\n        },\n        sendDate(closePicker = true)\n        {\n            const me = this;\n            if (me.format !== undefined)\n            {\n                if (me.isUTC)\n                {\n                    me.$emit('input', me.calcDate.utc().format(me.format));\n                }\n                else\n                {\n                    me.$emit('input', me.calcDate.format(me.format));\n                }\n            }\n            else\n            if (me.isUTC)\n            {\n                me.$emit('input', me.calcDate.utc());\n            }\n            else\n            {\n                me.$emit('input', me.calcDate);\n            }\n            if (closePicker)\n            {\n                me.showPicker = false;\n            }\n        },\n        changeDate(instance)\n        {\n            const me = this;\n\n            if (instance.active !== undefined && instance.active === false)\n            {\n                return;\n            }\n\n            me.calcDate = moment(instance.date);\n        },\n        clearPicker()\n        {\n            const me = this;\n            me.$emit('input', null);\n        },\n        calculateCalendar()\n        {\n            const me = this;\n            const setDate = me.calcDate;\n            let date = moment();\n            let i;\n\n            let startDayMonth = moment(setDate).startOf('month').format('d') - 1;\n            let endDayMonth = moment(setDate).endOf('month').format('d') - 1;\n\n            if (startDayMonth === -1)\n            {\n                startDayMonth = 6;\n            }\n            if (endDayMonth === -1)\n            {\n                endDayMonth = 6;\n            }\n\n            const daysInMonth = moment(setDate).daysInMonth();\n            const startDate = moment(setDate).startOf('month');\n            const endDate = moment(setDate).endOf('month');\n\n            const days = [];\n            for (i = startDayMonth; i > 0; i -= 1)\n            {\n                date = moment(startDate).subtract(i, 'days');\n\n                let active = true;\n                if (me.isStart && me.endCalDate < date)\n                {\n                    active = false;\n                }\n\n                if (me.isEnd && me.startCalDate > date)\n                {\n                    active = false;\n                }\n\n                days.push(\n                    {\n                        date,\n                        text: date.format('DD'),\n                        active,\n                        class: 'previous-month',\n                    },\n                );\n            }\n\n            for (i = 0; i < daysInMonth; i += 1)\n            {\n                date = moment(startDate).add(i, 'days');\n\n                let active = true;\n                if (me.isStart && me.endCalDate < date)\n                {\n                    active = false;\n                }\n\n                if (me.isEnd && me.startCalDate > date)\n                {\n                    active = false;\n                }\n\n                days.push(\n                    {\n                        date,\n                        text: date.format('DD'),\n                        active,\n                        class: `current-month${(date.format('YYYY-MM-DD') === setDate.format('YYYY-MM-DD')) ? ' active' : ''}`,\n                    },\n                );\n            }\n\n            let x = 1;\n            for (i = endDayMonth; i < 6; i += 1)\n            {\n                date = moment(endDate).add(x, 'days');\n\n                let active = true;\n                if (me.isStart && me.endCalDate < date)\n                {\n                    active = false;\n                }\n\n                if (me.isEnd && me.startCalDate > date)\n                {\n                    active = false;\n                }\n\n                days.push(\n                    {\n                        date,\n                        text: date.format('DD'),\n                        active,\n                        class: 'next-month',\n                    },\n                );\n                x += 1;\n            }\n\n            me.monthDays = [];\n\n            let j; let temparray; const\n                chunk = 7;\n            for (i = 0, j = days.length; i < j; i += chunk)\n            {\n                temparray = days.slice(i, i + chunk);\n                me.monthDays.push(temparray);\n            }\n        },\n    },\n    computed:\n    {\n        displayValue:\n        {\n            get()\n            {\n                const me = this;\n\n                if (me.displayFormat !== undefined)\n                {\n                    if (me.value == null || me.value === undefined)\n                    {\n                        return me.value;\n                    }\n                    if (me.format)\n                    {\n                        if (me.isUTC)\n                        {\n                            return moment.utc(me.value, me.format).local().format(me.displayFormat);\n                        }\n                        return moment(me.value, me.format).format(me.displayFormat);\n                    }\n                    if (me.isUTC)\n                    {\n                        return moment.utc(me.value).local().format(me.displayFormat);\n                    }\n                    return moment(me.value).format(me.displayFormat);\n                }\n                return me.value;\n            },\n            set(newValue)\n            {\n                const me = this;\n\n                if (moment(newValue, me.displayFormat, true).isValid())\n                {\n                    const sel = me.$refs.lb_input.selectionStart;\n                    me.calcDate = moment(newValue, me.displayFormat);\n                    me.sendDate(false);\n                    me.$nextTick(() =>\n                    {\n                        me.$refs.lb_input.setSelectionRange(sel, sel);\n                    });\n                }\n            },\n        },\n        month()\n        {\n            const me = this;\n            return me.translate(me.calcDate.format('MMMM').toUpperCase());\n        },\n        months()\n        {\n            const me = this;\n            const months = [];\n\n            const currMonth = moment(me.calcDate).month();\n            for (let i = 0; i < 12; i += 1)\n            {\n                const date = moment(me.calcDate).month(i);\n\n                let active = true;\n                if (me.isStart && me.endCalDate < date)\n                {\n                    active = false;\n                }\n\n                if (me.isEnd && me.startCalDate > date)\n                {\n                    active = false;\n                }\n\n                months.push({\n                    text: me.translate(date.format('MMM').toUpperCase()),\n                    date,\n                    active,\n                    selected: (currMonth === i),\n                });\n            }\n\n            return months;\n        },\n        years()\n        {\n            const me = this;\n            const years = [];\n\n            const currYear = moment(me.calcDate).year();\n            for (let i = me.minYearDef; i < me.maxYearDef; i += 1)\n            {\n                const date = moment(me.calcDate).year(i);\n\n                let active = true;\n                if (me.isStart && me.endCalDate < date)\n                {\n                    active = false;\n                }\n\n                if (me.isEnd && me.startCalDate > date)\n                {\n                    active = false;\n                }\n\n                years.push({\n                    text: date.format('YYYY'),\n                    date,\n                    active,\n                    selected: (currYear === i),\n                });\n            }\n\n            return years;\n        },\n        year()\n        {\n            const me = this;\n            return me.calcDate.format('YYYY');\n        },\n        hours()\n        {\n            const me = this;\n            const hours = [];\n            const currHour = moment(me.calcDate).hour();\n\n            for (let i = 0; i < 24; i += 1)\n            {\n                const date = moment(me.calcDate).hour(i);\n                let active = true;\n                if (me.isStart && me.endCalDate < date)\n                {\n                    active = false;\n                }\n\n                if (me.isEnd && me.startCalDate > date)\n                {\n                    active = false;\n                }\n\n                // check if hours is already in array (indicates daylight saving)\n                if (hours.slice(-1)[0] && hours.slice(-1)[0].text === date.format('HH'))\n                {\n                    hours.splice(-1);\n                }\n\n                hours.push({\n                    text: date.format('HH'),\n                    date,\n                    active,\n                    selected: (currHour === i),\n                });\n            }\n            return hours;\n        },\n        minutes()\n        {\n            const me = this;\n            const minutes = [];\n\n            const currMin = moment(me.calcDate).minute();\n            for (let i = 0; i < 60; i += 1)\n            {\n                const date = moment(me.calcDate).minute(i);\n\n                let active = true;\n                if (me.isStart && me.endCalDate < date)\n                {\n                    active = false;\n                }\n\n                if (me.isEnd && me.startCalDate > date)\n                {\n                    active = false;\n                }\n                minutes.push({\n                    text: date.format('mm'),\n                    date,\n                    active,\n                    selected: (currMin === i),\n                });\n            }\n\n            return minutes;\n        },\n        seconds()\n        {\n            const me = this;\n            const seconds = [];\n\n            const currSec = moment(me.calcDate).second();\n            for (let i = 0; i < 60; i += 1)\n            {\n                const date = moment(me.calcDate).second(i);\n                let active = true;\n                if (me.isStart && me.endCalDate < date)\n                {\n                    active = false;\n                }\n\n                if (me.isEnd && me.startCalDate > date)\n                {\n                    active = false;\n                }\n                seconds.push({\n                    text: date.format('ss'),\n                    date,\n                    active,\n                    selected: (currSec === i),\n                });\n            }\n\n            return seconds;\n        },\n    },\n};\n</script>\n\n<style lang=\"scss\" scoped>\n\n$primary-color:     #004C65;\n$secondary-color:   #F7DB6C;\n$white-color:       rgba(255, 255, 255, 0.90);\n\n// max width media query\n@mixin maxWidthBreakpoint($breakpoint) {\n    @media only screen and (max-width:($breakpoint/16)+em) {\n        @content\n    };\n}\n\n// max height media query\n@mixin maxHeightBreakpoint($breakpoint) {\n    @media only screen and (max-height:($breakpoint/16)+em) {\n        @content\n    };\n}\n\n// min width media query\n@mixin minWidthBreakpoint($breakpoint) {\n    @media only screen and (min-width:($breakpoint/16)+em) {\n        @content\n    };\n}\n\n// min and max media query\n@mixin minmaxWidthBreakpoint($minBreakpoint, $maxBreakpoint) {\n    @media only screen and (min-width:($minBreakpoint/16)+em) and (max-width:($maxBreakpoint/16)+em) {\n        @content\n    };\n}\n\n.lb-vue-cal_calendar {\n    all: initial;\n    font-family: 'Roboto', sans-serif;\n    position: relative;\n\n    .lb-vue-cal_overlay {\n        position: fixed;\n        width: 100%;\n        height: 100%;\n        top: 0px;\n        left: 0px;\n        z-index: 10000;\n        background-color: rgba($color: #000000, $alpha: 0.5)\n    }\n    .simple_cal_cont {\n        position: fixed;\n        z-index: 2;\n    }\n    .simple_cal {\n\n        display: inline-block;\n        background-color: #ffffff;\n\n        .lb-vue-cal_calendar\n        {\n            position: relative;\n            top: calc(100% + 5px);\n            left: 0px;\n            margin: 0px;\n            width: 400px;\n            display: block;\n        }\n    }\n    .clear {\n        position:absolute;\n        right:0;\n        top:0;\n        width:20px;\n        border:none;\n        outline:none;\n        background-color:#ffffff;\n        margin-right:2px;\n        cursor: pointer;\n    }\n\n    .lb-vue-cal_calendar {\n        position: fixed;\n        width: 400px;\n        //height: 500px;\n        top: calc((100% - 520px) / 2);\n        left: 50%;\n        z-index: 10001;\n        margin-left: -250px;\n        background-color: #ffffff;\n\n        @include maxWidthBreakpoint(600) {\n            left: 15%;\n            margin-left: 0;\n        }\n\n        @include maxWidthBreakpoint(500) {\n            left: 10%;\n        }\n\n        @include maxWidthBreakpoint(460) {\n            left: 6%;\n        }\n\n        @include maxWidthBreakpoint(400) {\n            left: 2.5%;\n            width: 95%;\n            margin: auto;\n            display: block;\n            text-align: center;\n            align-items: center;\n        }\n\n        .lb-vue-cal_row {\n            display: flex;\n\n            .lb-vue-cal_arrow {\n                cursor: pointer;\n            }\n\n            .active {\n                background-color: $primary-color;\n                color: $white-color;\n                border-radius: 6px;\n            }\n\n        }\n    }\n\n    & .calendar-head {\n        background-color: $primary-color;\n        height: 3rem;\n        color: $white-color;\n        align-items: center;\n        justify-content: space-between;\n        padding: 0 1rem;\n\n        & .month {\n\n            cursor: pointer;\n\n            & div {\n                float: left;\n            }\n        }\n\n        & .drop-down-arrow {\n            margin-left: .5rem;\n\n            & img {\n                margin-bottom: .1rem;\n            }\n        }\n\n        & .year {\n\n            cursor: pointer;\n\n            & div {\n                float: left;\n            }\n        }\n    }\n\n    & .inner-content {\n        padding: 0 1rem;\n        color: $primary-color;\n\n        & .days-name {\n            justify-content: space-evenly;\n            padding: 1.5rem 0;\n            font-weight: 400;\n            border-bottom: 1px solid rgba(185, 185, 185, 0.25);\n\n            & .calendar-days {\n                width: 14.28571%;\n                display: inline-block;\n                text-align: center;\n            }\n        }\n\n        & .date-numbers {\n            font-weight: 600;\n\n            & .single-day {\n                display: inline-block;\n                width: 14.28571%;\n                line-height: 3rem;\n                text-align: center;\n                cursor: pointer;\n                font-size: 1.2rem;\n                font-weight: 300;\n                margin: .2rem 0;\n\n                &.disabled {\n                    opacity: .3;\n                }\n\n                &:hover {\n                    background-color: $primary-color;\n                    color: $white-color;\n                    transition: .3s;\n                    border-radius: 6px;\n                }\n            }\n\n            & .previous-month {\n                opacity: .3;\n                font-weight: 300;\n            }\n\n            & .next-month {\n                opacity: .3;\n                font-weight: 300;\n            }\n        }\n    }\n\n    & .button-row {\n        display: block !important;\n\n        & button {\n            float: right;\n            padding: .5rem 2rem;\n            border-radius: 4px;\n            background-color: $secondary-color;\n            border: 0;\n            color: rgba(0, 0, 0, 0.8);\n            font-weight: 600;\n            font-size: .8rem;\n            margin-top: 1.5rem;\n            margin-bottom: 1.5rem;\n            cursor: pointer;\n\n            &:hover {\n                background-color: #f5de82;\n                transition: .3s;\n            }\n            &:disabled {\n                pointer-events: none;\n                opacity: .65;\n            }\n        }\n    }\n\n    & .change-month {\n\n        & .calendar-head {\n            margin-bottom: 1rem;\n        }\n\n        & .single-month {\n            display: block;\n            width: 48%;\n            float: left;\n            text-align: center;\n            border: 1px solid rgba(185, 185, 185, 0.25);\n            margin-right: 1%;\n            padding: 1rem 0;\n            margin-bottom: .5rem;\n            border-radius: 6px;\n            color: $primary-color;\n            text-transform: capitalize;\n            cursor: pointer;\n            font-weight: 300;\n            font-size: 1.5rem;\n\n            &.disabled {\n                opacity: .3;\n            }\n\n            &:hover,&.active {\n                background-color: $primary-color;\n                color: $white-color;\n                transition: .3s;\n            }\n        }\n    }\n\n    & .change-year {\n        height: 500px;\n        & .inner-content {\n            overflow-y: auto;\n            height: 85%;\n            margin-top: 1rem;\n        }\n\n        & .single-year {\n            display: block;\n            //width: 48%;\n            //float: left;\n            text-align: center;\n            //border: 1px solid rgba(185, 185, 185, 0.25);\n            margin-right: 1%;\n            padding: 1rem 0;\n            margin-bottom: .5rem;\n            border-radius: 6px;\n            color: $primary-color;\n            text-transform: capitalize;\n            cursor: pointer;\n            font-weight: 300;\n            font-size: 1.5rem;\n\n            &.disabled {\n                opacity: .3;\n            }\n\n            &:hover {\n                font-weight: 600;\n                transition: .1s;\n                font-size: 1.7rem;\n            }\n\n            &.active {\n                background-color: $primary-color;\n                color: $white-color;\n            }\n        }\n    }\n\n    & .content-time {\n        display: flex;\n        justify-content: space-evenly;\n        height: 75%;\n        margin-top: 1rem;\n        height: 350px;\n\n        & .hours, & .minutes, & .seconds {\n            overflow-y: auto;\n            width: 5rem;\n            text-align: center;\n            cursor: pointer;\n\n            & div {\n                padding: .5rem 0;\n                font-weight: 300;\n                font-size: 1.5rem;\n                margin-right: .5rem;\n\n                & .active {\n                    background-color: $primary-color;\n                    border-radius: 6px;\n                    color: $white-color;\n                }\n\n                &:hover {\n                    font-weight: 600;\n                    transition: .1s;\n                    font-size: 1.7rem;\n                }\n            }\n        }\n    }\n\n    & .button-time {\n        margin-right: 1rem;\n    }\n}\n\n.clear10 {\n    display: block;\n    height: 10px;\n}\n\n::-webkit-scrollbar {\n    width: 8px;\n  }\n\n  /* Track */\n  ::-webkit-scrollbar-track {\n    background: #efefef;\n    border-radius: 10px;\n  }\n\n  /* Handle */\n  ::-webkit-scrollbar-thumb {\n    background: $secondary-color;\n    border-radius: 10px;\n  }\n\n  /* Handle on hover */\n  ::-webkit-scrollbar-thumb:hover {\n    background: $primary-color;\n    transition: .3s;\n  }\n\n</style>\n",".lb-vue-cal_calendar {\n  all: initial;\n  font-family: 'Roboto', sans-serif;\n  position: relative; }\n  .lb-vue-cal_calendar .lb-vue-cal_overlay {\n    position: fixed;\n    width: 100%;\n    height: 100%;\n    top: 0px;\n    left: 0px;\n    z-index: 10000;\n    background-color: rgba(0, 0, 0, 0.5); }\n  .lb-vue-cal_calendar .simple_cal_cont {\n    position: fixed;\n    z-index: 2; }\n  .lb-vue-cal_calendar .simple_cal {\n    display: inline-block;\n    background-color: #ffffff; }\n    .lb-vue-cal_calendar .simple_cal .lb-vue-cal_calendar {\n      position: relative;\n      top: calc(100% + 5px);\n      left: 0px;\n      margin: 0px;\n      width: 400px;\n      display: block; }\n  .lb-vue-cal_calendar .clear {\n    position: absolute;\n    right: 0;\n    top: 0;\n    width: 20px;\n    border: none;\n    outline: none;\n    background-color: #ffffff;\n    margin-right: 2px;\n    cursor: pointer; }\n  .lb-vue-cal_calendar .lb-vue-cal_calendar {\n    position: fixed;\n    width: 400px;\n    top: calc((100% - 520px) / 2);\n    left: 50%;\n    z-index: 10001;\n    margin-left: -250px;\n    background-color: #ffffff; }\n    @media only screen and (max-width: 37.5em) {\n      .lb-vue-cal_calendar .lb-vue-cal_calendar {\n        left: 15%;\n        margin-left: 0; } }\n    @media only screen and (max-width: 31.25em) {\n      .lb-vue-cal_calendar .lb-vue-cal_calendar {\n        left: 10%; } }\n    @media only screen and (max-width: 28.75em) {\n      .lb-vue-cal_calendar .lb-vue-cal_calendar {\n        left: 6%; } }\n    @media only screen and (max-width: 25em) {\n      .lb-vue-cal_calendar .lb-vue-cal_calendar {\n        left: 2.5%;\n        width: 95%;\n        margin: auto;\n        display: block;\n        text-align: center;\n        align-items: center; } }\n    .lb-vue-cal_calendar .lb-vue-cal_calendar .lb-vue-cal_row {\n      display: flex; }\n      .lb-vue-cal_calendar .lb-vue-cal_calendar .lb-vue-cal_row .lb-vue-cal_arrow {\n        cursor: pointer; }\n      .lb-vue-cal_calendar .lb-vue-cal_calendar .lb-vue-cal_row .active {\n        background-color: #004C65;\n        color: rgba(255, 255, 255, 0.9);\n        border-radius: 6px; }\n  .lb-vue-cal_calendar .calendar-head {\n    background-color: #004C65;\n    height: 3rem;\n    color: rgba(255, 255, 255, 0.9);\n    align-items: center;\n    justify-content: space-between;\n    padding: 0 1rem; }\n    .lb-vue-cal_calendar .calendar-head .month {\n      cursor: pointer; }\n      .lb-vue-cal_calendar .calendar-head .month div {\n        float: left; }\n    .lb-vue-cal_calendar .calendar-head .drop-down-arrow {\n      margin-left: .5rem; }\n      .lb-vue-cal_calendar .calendar-head .drop-down-arrow img {\n        margin-bottom: .1rem; }\n    .lb-vue-cal_calendar .calendar-head .year {\n      cursor: pointer; }\n      .lb-vue-cal_calendar .calendar-head .year div {\n        float: left; }\n  .lb-vue-cal_calendar .inner-content {\n    padding: 0 1rem;\n    color: #004C65; }\n    .lb-vue-cal_calendar .inner-content .days-name {\n      justify-content: space-evenly;\n      padding: 1.5rem 0;\n      font-weight: 400;\n      border-bottom: 1px solid rgba(185, 185, 185, 0.25); }\n      .lb-vue-cal_calendar .inner-content .days-name .calendar-days {\n        width: 14.28571%;\n        display: inline-block;\n        text-align: center; }\n    .lb-vue-cal_calendar .inner-content .date-numbers {\n      font-weight: 600; }\n      .lb-vue-cal_calendar .inner-content .date-numbers .single-day {\n        display: inline-block;\n        width: 14.28571%;\n        line-height: 3rem;\n        text-align: center;\n        cursor: pointer;\n        font-size: 1.2rem;\n        font-weight: 300;\n        margin: .2rem 0; }\n        .lb-vue-cal_calendar .inner-content .date-numbers .single-day.disabled {\n          opacity: .3; }\n        .lb-vue-cal_calendar .inner-content .date-numbers .single-day:hover {\n          background-color: #004C65;\n          color: rgba(255, 255, 255, 0.9);\n          transition: .3s;\n          border-radius: 6px; }\n      .lb-vue-cal_calendar .inner-content .date-numbers .previous-month {\n        opacity: .3;\n        font-weight: 300; }\n      .lb-vue-cal_calendar .inner-content .date-numbers .next-month {\n        opacity: .3;\n        font-weight: 300; }\n  .lb-vue-cal_calendar .button-row {\n    display: block !important; }\n    .lb-vue-cal_calendar .button-row button {\n      float: right;\n      padding: .5rem 2rem;\n      border-radius: 4px;\n      background-color: #F7DB6C;\n      border: 0;\n      color: rgba(0, 0, 0, 0.8);\n      font-weight: 600;\n      font-size: .8rem;\n      margin-top: 1.5rem;\n      margin-bottom: 1.5rem;\n      cursor: pointer; }\n      .lb-vue-cal_calendar .button-row button:hover {\n        background-color: #f5de82;\n        transition: .3s; }\n      .lb-vue-cal_calendar .button-row button:disabled {\n        pointer-events: none;\n        opacity: .65; }\n  .lb-vue-cal_calendar .change-month .calendar-head {\n    margin-bottom: 1rem; }\n  .lb-vue-cal_calendar .change-month .single-month {\n    display: block;\n    width: 48%;\n    float: left;\n    text-align: center;\n    border: 1px solid rgba(185, 185, 185, 0.25);\n    margin-right: 1%;\n    padding: 1rem 0;\n    margin-bottom: .5rem;\n    border-radius: 6px;\n    color: #004C65;\n    text-transform: capitalize;\n    cursor: pointer;\n    font-weight: 300;\n    font-size: 1.5rem; }\n    .lb-vue-cal_calendar .change-month .single-month.disabled {\n      opacity: .3; }\n    .lb-vue-cal_calendar .change-month .single-month:hover, .lb-vue-cal_calendar .change-month .single-month.active {\n      background-color: #004C65;\n      color: rgba(255, 255, 255, 0.9);\n      transition: .3s; }\n  .lb-vue-cal_calendar .change-year {\n    height: 500px; }\n    .lb-vue-cal_calendar .change-year .inner-content {\n      overflow-y: auto;\n      height: 85%;\n      margin-top: 1rem; }\n    .lb-vue-cal_calendar .change-year .single-year {\n      display: block;\n      text-align: center;\n      margin-right: 1%;\n      padding: 1rem 0;\n      margin-bottom: .5rem;\n      border-radius: 6px;\n      color: #004C65;\n      text-transform: capitalize;\n      cursor: pointer;\n      font-weight: 300;\n      font-size: 1.5rem; }\n      .lb-vue-cal_calendar .change-year .single-year.disabled {\n        opacity: .3; }\n      .lb-vue-cal_calendar .change-year .single-year:hover {\n        font-weight: 600;\n        transition: .1s;\n        font-size: 1.7rem; }\n      .lb-vue-cal_calendar .change-year .single-year.active {\n        background-color: #004C65;\n        color: rgba(255, 255, 255, 0.9); }\n  .lb-vue-cal_calendar .content-time {\n    display: flex;\n    justify-content: space-evenly;\n    height: 75%;\n    margin-top: 1rem;\n    height: 350px; }\n    .lb-vue-cal_calendar .content-time .hours, .lb-vue-cal_calendar .content-time .minutes, .lb-vue-cal_calendar .content-time .seconds {\n      overflow-y: auto;\n      width: 5rem;\n      text-align: center;\n      cursor: pointer; }\n      .lb-vue-cal_calendar .content-time .hours div, .lb-vue-cal_calendar .content-time .minutes div, .lb-vue-cal_calendar .content-time .seconds div {\n        padding: .5rem 0;\n        font-weight: 300;\n        font-size: 1.5rem;\n        margin-right: .5rem; }\n        .lb-vue-cal_calendar .content-time .hours div .active, .lb-vue-cal_calendar .content-time .minutes div .active, .lb-vue-cal_calendar .content-time .seconds div .active {\n          background-color: #004C65;\n          border-radius: 6px;\n          color: rgba(255, 255, 255, 0.9); }\n        .lb-vue-cal_calendar .content-time .hours div:hover, .lb-vue-cal_calendar .content-time .minutes div:hover, .lb-vue-cal_calendar .content-time .seconds div:hover {\n          font-weight: 600;\n          transition: .1s;\n          font-size: 1.7rem; }\n  .lb-vue-cal_calendar .button-time {\n    margin-right: 1rem; }\n\n.clear10 {\n  display: block;\n  height: 10px; }\n\n::-webkit-scrollbar {\n  width: 8px; }\n\n/* Track */\n::-webkit-scrollbar-track {\n  background: #efefef;\n  border-radius: 10px; }\n\n/* Handle */\n::-webkit-scrollbar-thumb {\n  background: #F7DB6C;\n  border-radius: 10px; }\n\n/* Handle on hover */\n::-webkit-scrollbar-thumb:hover {\n  background: #004C65;\n  transition: .3s; }\n\n/*# sourceMappingURL=DateTimePicker.vue.map */"]}, media: undefined });

      };
      /* scoped */
      var __vue_scope_id__ = "data-v-435a2476";
      /* module identifier */
      var __vue_module_identifier__ = undefined;
      /* functional template */
      var __vue_is_functional_template__ = false;
      /* style inject SSR */
      

      
      var component = normalizeComponent_1(
        { render: __vue_render__, staticRenderFns: __vue_staticRenderFns__ },
        __vue_inject_styles__,
        __vue_script__,
        __vue_scope_id__,
        __vue_is_functional_template__,
        __vue_module_identifier__,
        browser,
        undefined
      );

    // Import vue component

    exports.default = component;

    Object.defineProperty(exports, '__esModule', { value: true });

})));
