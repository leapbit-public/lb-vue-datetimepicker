// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue';
import DateTimePicker from '@/DateTimePicker';
import Dev from './Dev.vue';
// import DateTimePicker from '../dist/lb-vue-datetimepicker'

Vue.component('datetimepicker', DateTimePicker);

new Vue({
    render: (h) => h(Dev),
}).$mount('#app');
