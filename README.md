<br><br>

<h3>lb-vue-datetimepicker</h3>

<br>
<p><img src="https://www.leapbit.com/branding/logo.png" style="width: 300px;"></p>

## Demo

<a href="https://codesandbox.io/s/lb-vue-datetimepicker-n209y">Demo</a>

<br>

## Screenshot
<p>Datetime Picker distinguishes two display modes: regular and simple. </p>
<p><img src="https://gitlab.com/leapbit-public/lb-vue-datetimepicker/raw/master/screen_0.png" alt="lb-vue-datetimepicker" ></p>
<p><img src="https://gitlab.com/leapbit-public/lb-vue-datetimepicker/raw/master/screen_1.png" alt="lb-vue-datetimepicker" ></p>
<p><img src="https://gitlab.com/leapbit-public/lb-vue-datetimepicker/raw/master/screen_2.png" alt="lb-vue-datetimepicker" ></p>
<p>Simple mode displays Datetime Picker select box below date input control: </p>
<p><img src="https://gitlab.com/leapbit-public/lb-vue-datetimepicker/raw/master/screen_3.png" alt="lb-vue-datetimepicker" ></p>
## Description

<p align="center">
  Vue2.js date time picker using moment.js as a date management library
</p>

<br><br><br><br><br>

## Install

``` bash
npm install lb-vue-datetimepicker --save
```

Include plugin in your `main.js` file.

```js
import Vue from 'vue'
import lbVueDateTimePicker from 'lb-vue-datetimepicker';
Vue.component('lb-vue-datetimepicker', lbVueDateTimePicker);
```

## Usage

# Minimum required data

```html
<template>
    <datetimepicker v-model="date"></datetimepicker>
</template>
```

# With all available options
'inputClass', 'translateMethod', 'type', 'format', 'display-format', 'isUTC', 'isClearable'
```html
<template>
    <datetimepicker v-model="date" input-class="form-control" translate-method="translateKeys" type="datetime" format="YYYY-MM-DD HH:mm:ss" display-format="DD.MM.YYYY." :isUTC="true" :isClearable="true"></datetimepicker>
</template>
```
<br><br>

# Simple mode
'simple'
```html
<template>
    <datetimepicker v-model="date" input-class="form-control" translate-method="translateKeys" type="datetime" format="YYYY-MM-DD HH:mm:ss" display-format="DD.MM.YYYY." :simple="true" :isUTC="true"></datetimepicker>
</template>
```
<br><br>

# Start end date with two pickers
'start', 'end'
```html
<template>
    <datetimepicker v-model="startDate" :end="endDate" input-class="form-control" translate-method="translateKeys" type="datetime" format="YYYY-MM-DD HH:mm:ss" display-format="DD.MM.YYYY." :simple="true" :isUTC="true"></datetimepicker>
    <datetimepicker v-model="endDate" :start="startDate" input-class="form-control" translate-method="translateKeys" type="datetime" format="YYYY-MM-DD HH:mm:ss" display-format="DD.MM.YYYY." :simple="true" :isUTC="true"></datetimepicker>
</template>
```
<br><br>

## Available props

| Property | Type | Required | Description |
| :---------------- | :-- | :-- | :-- |
| value (v-model) | string or date/moment object | * | date value |
| input-class | string | | Input field class |
| translate-method | function | | If you want to translate the months and buttons this function will be called with translation key |
| type | string | | type of picker, can be: date, datetime, time |
| format | string | | Moment format of the date that will be actual value, if not available it will use moment object |
| display-format | string | | Moment format of the date to display to user |
| simple | boolean | | If simple mode is true, it will show as a select box below input and you can change date in input |
| min-year | string | | Minimum year for selection (defaults to 1900) |
| max-year | string | | Maximum year for selection (defaults to 2100) |
|isUTC | boolean |  | If isUTC property is set true, selected local datetimes are converted to UTC. |
| start | string or date/moment object |  | start date value to limit selection or to use it on end date picker |
| end | string or date/moment object |  | end date value to limit selection or to use it on start date picker |
| isClearable | boolean | | cleareable datetime picker input |
| disabled | boolean | | disabled datetime picker input
<br><br>